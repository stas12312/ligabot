import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'mytest.settings')


celery_app = Celery('mytest')
celery_app .config_from_object('django.conf:settings', namespace='CELERY')
celery_app.conf.timezone = os.environ.get('TIMEZONE', 'Asia/Novosibirsk'),
celery_app .autodiscover_tasks()
