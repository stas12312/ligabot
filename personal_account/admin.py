from django.contrib import admin
from .models import Code


@admin.register(Code)
class CodeAdmin(admin.ModelAdmin):
    list_display = ('id', 'teacher', 'code', 'datetime_created')
    list_display_links = ('id', 'code')
