from rest_framework import serializers
from telegrambot.models import Preparation, LogSending, Branch, Lesson, Teacher, Sort


class BranchSerializers(serializers.ModelSerializer):
    """Сериализация филиала"""
    class Meta:
        model = Branch
        fields = ("name",)


class LessonSerializers(serializers.ModelSerializer):
    """Сериализация занятия"""

    class Meta:
        model = Lesson
        fields = ("name", "start", "end", "day_of_weak")


class TeacherSerializers(serializers.ModelSerializer):
    """Сериализация преподавателя"""

    class Meta:
        model = Teacher
        fields = ("full_name",)


class PreparationSerializers(serializers.ModelSerializer):
    """Сериализация подготовок"""

    class Meta:
        model = Preparation
        fields = ("date", "time",)


class LogSerializers(serializers.ModelSerializer):
    """Сериализация отчетов кабинетов"""
    branch = BranchSerializers()
    lesson = LessonSerializers()

    class Meta:
        model = LogSending
        fields = ("date", "time", "time_before", "branch", "lesson")


class TopLogSerializers(serializers.ModelSerializer):
    """Сериализация логов для топа"""
    teacher = TeacherSerializers()

    class Meta:
        model = LogSending
        fields = ("teacher", "time_before")


class SortSerializers(serializers.ModelSerializer):
    """Сериализация сортировки"""

    branch = BranchSerializers()

    class Meta:
        model = Sort
        fields = ("status", "comment", "date", "time", "branch")
