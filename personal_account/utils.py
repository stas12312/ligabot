from .models import Token
from telegrambot.models import Teacher
from datetime import timedelta, datetime
from django.utils import timezone
import calendar


def get_month_info(year, month):
    """
    Функция для определения начала и конца учета подготовок
    :param month: Номер месяца
    :param year: Год
    :return: Кортеж из двух дат: Начало недели и конец недели
    """
    calendar_ = calendar.Calendar()
    info_weaks = calendar_.monthdatescalendar(year, month)
    date_start = info_weaks[0][0]  # Первый день первой недели
    if info_weaks[-1][-1].month != month:
        date_end = info_weaks[-2][-1]
    else:
        date_end = info_weaks[-1][-1]

    count = len([1 for i in calendar.monthcalendar(year, month) if i[5] != 0])  # Количество рабоичх выходных
    return date_start, date_end, count


def check_token(access_token):
    try:
        # Получаем объект токена из БД
        token_access = Token.objects.get(accessToken=access_token)

        # Проверяем, что токен действительный
        if timezone.now() > token_access.expireDateTime:
            token_access.delete()
            return False
        else:
            token_access.update_expire()  # Обновляем дату окончания срока действия токена
            return token_access.teacher
    except Token.DoesNotExist:
        return False