from django.db import models
from telegrambot.models import Teacher
import uuid
from django.utils import timezone
from datetime import timedelta


class Code(models.Model):
    """
    Хранение кодов подтверждения
    """
    teacher = models.ForeignKey(Teacher, verbose_name='Пеподаватель', on_delete=models.CASCADE)
    code = models.CharField(verbose_name='Код', max_length=10)
    datetime_created = models.DateTimeField(auto_now_add=True, verbose_name='Дата и время создания', null=True)
    teacher_uuid = models.UUIDField(default=uuid.uuid4, editable=False, verbose_name='Разовый UUID')
    attempts = models.SmallIntegerField(default=3, verbose_name='Количество попыток')
    # message_id = models.IntegerField(editable=False)  # message_id для удаление кода

    class Meta:
        verbose_name = 'Код подтверждения'
        verbose_name_plural = 'Коды подтверждения'


class Token(models.Model):
    teacher = models.ForeignKey(Teacher, verbose_name='Преподаватель', on_delete=models.CASCADE)
    accessToken = models.CharField(max_length=256, verbose_name='Токен')
    createDateTime = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    expireDateTime = models.DateTimeField(verbose_name='Срок годности')

    def update_expire(self):
        self.expireDateTime = timezone.now() + timedelta(days=1)
        self.save()

