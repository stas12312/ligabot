from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework import permissions
from .models import Code
from telegrambot.models import Teacher, Preparation, LogSending, Sort
from telegrambot.bot import bot
from .models import Code, Token
import json
import secrets
from datetime import datetime, timedelta
from .serializers import PreparationSerializers, LogSerializers, TopLogSerializers, SortSerializers
import calendar
from rest_framework.parsers import JSONParser
from . import utils
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers


class PrepareAPI(APIView):
    """API для получении информации о подготовок"""
    permission_classes = [permissions.AllowAny, ]

    def get(self, request):
        try:
            token = request.META['HTTP_AUTHORIZATION']
            teacher = utils.check_token(token)
            prepares = Preparation.objects.filter(teacher=teacher)[:10]
            serializer_prepares = PreparationSerializers(prepares, many=True)

            return Response({'status': 'ok', 'prepares': serializer_prepares.data})
        except Exception as e:
            return Response({'status': 'error'})


class SortingAPI(APIView):
    """API для получение информации о сортировках"""
    permission_classes = [permissions.AllowAny, ]

    def get(self, request):
        try:
            token = request.META['HTTP_AUTHORIZATION']
            teacher = utils.check_token(token)
            print(teacher)
            sorting = Sort.objects.filter(teacher=teacher)[:10]
            sorting_serializers = SortSerializers(sorting, many=True)
            return Response({'status': 'ok', 'sortings': sorting_serializers.data})
        except Exception as e:
            return Response({'status', 'error'})


class LogAPI(APIView):
    """API для информации об отчетов кабинета"""

    permission_classes = [permissions.AllowAny, ]

    def get(self, request):
        try:
            token = request.META['HTTP_AUTHORIZATION']
            teacher = utils.check_token(token)
            logs = LogSending.objects.filter(teacher=teacher)[:10]
            serializers_logs = LogSerializers(logs, many=True)
            return Response({'status': 'ok', 'logs': serializers_logs.data})
        except Exception as e:
            return Response({'status': 'error'})


class StatisticAPI(APIView):
    permission_classes = [permissions.AllowAny, ]

    def get(self, request):
        token = request.META['HTTP_AUTHORIZATION']
        teacher = utils.check_token(token)
        if not teacher:
            return Response({'status': 'error', 'error': {'code': '0001', 'detail': 'Нет прав для доступа'}})
        now_datetime = datetime.now()

        # Получаем информацию о начале и конце месяца
        start, end, count = utils.get_month_info(now_datetime.year, now_datetime.month)
        count_prepare = Preparation.objects.filter(teacher=teacher, date__gte=start, date__lte=end).count()
        # ТОП
        # Топ по прибытию
        # Считаем среде время прибытие для преподавателей
        arg_list = []
        teachers = Teacher.objects.filter(is_active=True)  # Получаем всех активных преподавателей
        for teacher in teachers:
            # Получаем время до начала занятия для текущего месяца
            time_befores = list(
                LogSending.objects.values_list(
                    'time_before', flat=True).filter(
                    date__month=now_datetime.month,
                    date__year=now_datetime.year,
                    teacher=teacher,
                )
            )
            try:
                # Считаем среднее время до начала занятия
                average = int(sum(time_befores) / len(time_befores))
                l_full_name = teacher.full_name.split()
                sec_full_name = l_full_name[0] + ' ' + l_full_name[1] + ' ' + l_full_name[2][0] + '.'
                arg_list.append({'teacher': {'full_name': sec_full_name}, 'time_before': average})
            except:
                pass

        time_arrival_teachers = LogSending.objects.filter(
            date__month=now_datetime.month,
            date__year=now_datetime.year,
            time_before__isnull=False).order_by('-time_before')[:5]
        arg_list = sorted(arg_list, key=lambda x: x['time_before'], reverse=True)
        serializers_top_time_arrival = TopLogSerializers(time_arrival_teachers, many=True)
        return Response({'status': 'ok',
                         'statistic':
                             {'count_prepare': count_prepare, 'expected_prepare': count},
                         'top':
                             {'time_arrival': arg_list[:10]}
                         })


@csrf_exempt
@api_view(['POST'])
@permission_classes((permissions.AllowAny,))
def check_teacher(request):
    """
    Проверка существавания пользователя и отправка кода для подтверждения
    :param request:
    :return:
    """
    data = json.loads(request.read())
    if 'name' in data:
        name = data['name']
        try:
            teacher = Teacher.objects.get(
                full_name__iexact=name,
                is_active=True,
                chat_id__isnull=False)

            # Если существует сгенерированый код, то удаляем его
            try:
                code = Code.objects.get(teacher=teacher)
                code.delete()
            except Code.DoesNotExist:
                pass

            code = Code(teacher=teacher, code=''.join(secrets.choice('123456789') for i in range(5)))
            code.save()
            bot.send_message(teacher.chat_id, f'Код подтверждения: {code.code}')
            return Response({'status': 'ok', 'teacher_id': code.teacher_uuid})
        except Teacher.DoesNotExist:
            return Response({'status': 'error', 'detail': 'Преподаватель {} не найден'.format(name)})
    if 'accept_code' in data:
        accept_code = data['accept_code']
        teacher_id = data['teacher_id']
        try:
            code = Code.objects.get(teacher_uuid=teacher_id)
            if code.code == accept_code:
                teacher = code.teacher
                code.delete()
                token = Token()
                token.teacher = teacher
                token.accessToken = secrets.token_hex(32)
                token.expireDateTime = datetime.now() + timedelta(days=1)
                token.save()
                return Response({'status': 'ok', 'token_access': token.accessToken})
            else:
                code.attempts -= 1
                code.save()
                if code.attempts == 0:
                    code.delete()
                    return Response({'status': 'error', 'detail': 'Слишком много неудачных попыток'})
                else:
                    return Response({'status': 'error', 'detail': f'Неверный код, осталось попыток: {code.attempts}'})

        except Code.DoesNotExist:
            return Response({'status': 'error', 'detail': 'Неверный код'})

    return Response({'status': 'error', 'detail': f'Неизвестный запрос'})


@csrf_exempt
@api_view(['GET'])
@permission_classes((permissions.AllowAny,))
def logout(request):
    try:
        token = request.META['HTTP_AUTHORIZATION']
        print(token)
        token = Token.objects.get(accessToken=token)
        token.delete()
        return Response({'status': 'ok'})
    except Exception as e:
        return Response({'status': 'error'})
