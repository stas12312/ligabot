function hasVerticalScroll(node) {
    if (node === undefined) {
        if (window.innerHeight)
            return document.body.offsetHeight > innerHeight;
        else
            return document.documentElement.scrollHeight >
                document.documentElement.offsetHeight ||
                document.body.scrollHeight > document.body.offsetHeight;
    } else {
        return node.scrollHeight > node.offsetHeight;
    }
}

$(document).ready(function () {
    let $popup = $('.b-popup');

    function hidePopUp() {
        $popup.fadeOut(200, function () {
            $("body").css({"overflow-y": "visible", 'padding-right': '0'});
        });

    }

    function showPopUp() {
        $popup.fadeIn(200);
        if (hasVerticalScroll()) {
            $("body").css({"overflow-y": "hidden", 'padding-right': '17px'})
        }
    }

    $('.img-popup').click(function () {
        let $img = $(this).attr("src");
        $('#b-popup-img').attr("src", $img);
        showPopUp();

    });

    $popup.click(function (e) {
        if ($(e.target).is('.b-popup')) {
            hidePopUp();
        }
    });
    let $body = $('body');

    //--- Показ следующего изображения ---//
    function show_next_img(e = null) {
        let src = $popup_img.attr("src");
        let $img = $('.img-popup[src="' + src + '"]');
        let attr;
        if ($img.prev('.img-popup').attr("src") === undefined) {
            let $container = $img.parent().children('.img-popup').last();
            attr = $container.attr("src");
        } else {
            attr = $img.prev('.img-popup').attr("src");
        }
        $popup_img.attr("src", attr);
    }

    //--- Показ предыдущего изображения ---//
    function show_prev_img(e) {
        let src = $popup_img.attr("src");
        let $img = $('.img-popup[src="' + src + '"]');
        let attr;
        if ($img.next('.img-popup').attr("src") === undefined) {
            let $container = $img.parent().children('.img-popup').first();
            attr = $container.attr("src");
        } else {
            attr = $img.next('.img-popup').attr("src");
        }
        $popup_img.attr("src", attr);
    }


    let $popup_img = $('#b-popup-img');
    $body.keydown(function (e) {
        // Нажатие ->
        if (e.keyCode === 39) {
            show_next_img();
            // Нажатие <-
        } else if (e.keyCode === 37) {
            show_prev_img();
            // Нажатие Esc
        } else if (e.keyCode === 27) {
            e.preventDefault();
            hidePopUp();
        }
    });

    // Переключение изображения по нажатию на картинку, либо на кнопку
    $('#next-img,.img-container').click(show_prev_img);

    // Переключение изоюражения по нажатию на кнопку
    $('#prev-img').click(show_next_img);


    $body.click(function (e) {
        if (!$(e.target).is($searchResult) && !$(e.target).is($serachField)) {
            $searchResult.fadeOut(200);
        }
    });
    $('.b-popup-close').click(function () {
        hidePopUp();
    });

    let $serachField = $('#search');
    let $searchResult = $('#search_result');
    let choice = null;
    let $select = $serachField;


    $serachField.keyup(function (e) {
        let key = e.keyCode;
        if (key === 40 || key === 38 || key === 39 || key === 37) {
            return false;
        }
        if ($serachField.val().length < 1) {
            $('#search_result').html('');
            choice = null;
            $select = null;
            return false;

        }
        $.ajax({
            type: 'GET',
            url: '/search',
            data: {
                'search_text': $serachField.val(),
            },
            success: searchSuccess,
            dataType: 'html'
        });
    });

    $serachField.focus(function () {
        $searchResult.fadeIn(200);
    });
    $serachField.keydown(function (e) {

        if (e.keyCode === 38) { //UP
            if ($(e.target).is($select)) {
            } else {
                if (!$select.prev('li').is('li')) {
                    $select.children('a').removeClass('search_item_selected');
                    $select = $serachField;
                } else {
                    $select.children('a').removeClass('search_item_selected');
                    $select = $select.prev('li');
                    $select.children('a').addClass('search_item_selected');
                }
            }
        } else if (e.keyCode === 40) { //DOWN
            if ($select.is($serachField)) {
                $select = $searchResult.children('li').first();
            } else {
                if (!$select.next('li').is('li')) {
                    return
                }
                $select.children('a').removeClass('search_item_selected');
                $select = $select.next('li');
            }
            $select.children('a').addClass('search_item_selected');

        } else if (e.keyCode === 13) { //ENTER
            if (!$select.is($serachField)) {
                location.href = $select.children('a').attr('href');
            }

        }
    });

    $searchResult.mouseover(function (e) {
        let $target = $(e.target);
        if ($target.is('a')) {
            $select.children('a').removeClass('search_item_selected');
            $select = $target.parent();
            $select.children('a').addClass('search_item_selected');
        }
    });

    $searchResult.mouseout(function () {
        $select.children('a').removeClass('search_item_selected');
        $select = $serachField;
    });

    function searchSuccess(data, teachers, jqXHR) {
        $select = $serachField;
        $searchResult.html(data);
    }

    /*  function digitalWatch() {
        var date = new Date();
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var seconds = date.getSeconds();
        if (day < 10) day = "0" + day;
        if (month < 10) month = "0" + month;
        if (hours < 10) hours = "0" + hours;
        if (minutes < 10) minutes = "0" + minutes;
        if (seconds < 10) seconds = "0" + seconds;
        document.getElementById("digital_watch").innerHTML = day + "." + month + "."+year+" "+hours + ":" + minutes + ":" + seconds;
      }

      digitalWatch();
      setInterval(digitalWatch,1000);
  */
    $('#jq-milt').multiSelect({
        // selectableHeader: "<div class=''>Список</div>",
        // selectionHeader: "<div class=''>Выбранные преподаватели</div>",
        selectableHeader: "<input type='text' class='form-control my-1' autocomplete='off' placeholder='Поиск'>",
        selectionHeader: "<input type='text' class='form-control my-1' autocomplete='off' placeholder='Поиск'>",
        afterInit: function (ms) {
            var that = this,
                $selectableSearch = that.$selectableUl.prev(),
                $selectionSearch = that.$selectionUl.prev(),
                selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                .on('keydown', function (e) {
                    if (e.which === 40) {
                        that.$selectableUl.focus();
                        return false;
                    }
                });

            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                .on('keydown', function (e) {
                    if (e.which == 40) {
                        that.$selectionUl.focus();
                        return false;
                    }
                });
        },
        afterSelect: function () {
            this.qs1.cache();
            this.qs2.cache();
        },
        afterDeselect: function () {
            this.qs1.cache();
            this.qs2.cache();
        }
    });

    $('#select-all').click(function () {
        $('#jq-milt').multiSelect('select_all');
        return false;
    });

    $('#deselect-all').click(function () {
        $('#jq-milt').multiSelect('deselect_all');
        return false;
    });

});

