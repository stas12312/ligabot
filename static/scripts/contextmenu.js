let $target;
        $(document).ready(function () {

            $(document).on('contextmenu click', function (e) {
                if($(e.target).is('li')){
                    let $tg = $(e.target);
                    let action = $tg.data('action');
                    console.log(action);

                    // Если удаление фото
                    if(action === 'dp'){
                        let id = $target.data('id');
                        let path = $target.attr('src');
                        let object = $target.data('object');
                        delete_image(id, path, $target, object);
                    }

                }
                $('.context-menu').remove();
                $target = null;
                if ($(e.target).is('.img-contextmenu')) {

                    e.preventDefault();
                    if (e.which === 3) {
                        console.log('Создание меню');
                        $target = $(e.target);
                        $('<div/>', {
                            'class': 'context-menu',
                        }).css({
                            left: e.pageX + 'px',
                            top: e.pageY + 'px',
                        }).appendTo('body').append(
                            $('<ul/>').append('<li data-action="dp">Удалить фото</li>')
                        ).show('fast')
                    }
                }


            });

            function delete_image(id, path, image, object){
                $.ajax({
                    url: '/utils/delete-image/',
                    method: 'GET',
                    dataType: 'json',
                    data: {
                        'object': object,
                        'path': path,
                        'id': id,
                    },
                    success: function(data){
                        console.log(data);
                        if (data.info.is_delete){

                            let table = image.parent().parent();
                            if(data.info.object === 'Сортировка'){
                                table.remove();
                                return;
                            }
                            table.removeClass('table-success').addClass('table-danger');
                            let ths = table.find('th');
                            console.log(ths);
                            for(let i = 1; i < 4; ++i){
                                $(ths[i]).text('-');
                            }
                            $(ths[3]).removeClass('text-left');
                        }
                        image.remove();
                    }
                })
            }
        });