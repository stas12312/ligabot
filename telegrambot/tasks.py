import json
from datetime import timedelta

from celery import shared_task
from django.db.models import Count

from telegrambot.bot import bot, send_message_for_admin
from telegrambot.models import *
from .bot import mailing_bot
from .models import Settings, Task
from .utils import TASK_SHOW_EXECUTE
from .utils import get_minutes, get_inline_keyboard, clear_text


def time_to_minute(time):
    """Перевод времени в минуты"""
    return time.hour * 60 + time.minute


@shared_task
def check_logs():
    """
    Уведомления о том, что не указан комментарий к фото
    :return:
    """
    message_for_uncertain = Settings.get_value('message_for_uncertain')
    now = timezone.localtime(timezone.now())
    now_date = datetime.date(now)
    now_time = datetime.time(now)
    if now_time.hour >= 22 or now_time.hour <= 8:
        return
    for tempLog in TempData.objects.all():
        if now_date != tempLog.date or (time_to_minute(now_time) - time_to_minute(tempLog.time)) >= 5:
            print(f'Отправка сообщения об отсутствии преподавателю {tempLog.teacher.full_name}')
            # Отправляем сообщение
            bot.send_message(tempLog.teacher.chat_id, message_for_uncertain, parse_mode='HTML')


@shared_task
def check_arrival():
    """Проверка прибытия преподавателя"""

    # Проверяем, что настройка включена
    on_danger = Settings.get_value('on_danger')
    if not on_danger:
        return

    time = timezone.localtime(timezone.now())

    day = time.weekday() + 1
    print(day)
    # Получаем филиалы, на которых сегодня занятия и занятие только будут
    branches = Branch.objects.filter(lessons__day_of_weak=day)

    # Пролучаем филиалы, на которых не было отчёта
    branches_without_log = []
    for branch in branches:
        if not LogSending.objects.filter(date=time, branch=branch).count():
            if branch not in branches_without_log:
                branches_without_log.append(branch)

    # Для каждого филиала, проверяем, было ли прибытие на филиал
    # если до первого занятия меньше 30 минут
    # а преподавателя нет, то отправляем сообщение администраторам
    for branch in branches_without_log:
        print(f'Филиал: {branch.name}')
        # Получаем первое занятие на филиале
        lesson = list(branch.lessons.all().order_by('start'))[0]
        print(f'{lesson.start} - {time}')
        delta = get_minutes(lesson.start) - get_minutes(time)
        print(delta)
        if 0 < delta < 30:
            print('Warning')
            msg = f'На филиал {branch} не прибыл преподаватель'
            send_message_for_admin('Внимание', msg)


@shared_task
def check_expired_tasks():
    # Получаем задачи, чьи дедлайны просрочены
    now = timezone.localtime(timezone.now())
    tasks = Task.objects.filter(deadline__date__lte=now, status=1)
    for task in tasks:
        # Клавиатура с кнопкой перейти к задаче
        keyboard = get_inline_keyboard([
            ('➡️ Перейти к задаче', [TASK_SHOW_EXECUTE, task.id])
        ])
        if task.executor:
            delta = now - task.deadline
            text = clear_text(task.text)
            if delta.days == 0:
                bot.send_message(task.executor.chat_id, f'*Напоминание*\n'
                                                        f'Сегодня у задачи "{text}" дедлайн',
                                 parse_mode='Markdown', reply_markup=keyboard)
            else:

                bot.send_message(task.executor.chat_id, f'*Напоминание*\n'
                                                        f'Задача "{text}" просрочена\n'
                                                        f'Кол-во просроченных дней: {delta.days}',
                                 parse_mode='Markdown', reply_markup=keyboard)


@shared_task
def check_mail():
    """Рассылка отложенных сообщений"""
    now = timezone.localtime(timezone.now())
    messages_for_sender = Message.objects.filter(datetime__lte=now, is_sender=False)
    for message in messages_for_sender:
        teachers_list = []
        for id_teacher in json.loads(message.detail):
            teachers_list.append(Teacher.objects.get(id=id_teacher))
        results = mailing_bot(message.title, message.body, teachers_list)
        message.detail = json.dumps(results[1])
        message.receivers = len(results[0]['success'])
        message.is_sender = True
        message.datetime = now
        message.save()


@shared_task
def delete_empty_orders():
    """
    Удаление заказов деталей, с момента создания которых
    прошло 24 часа и в которых нет деталей
     """
    now = timezone.localtime(timezone.now())
    safe_line = now - timedelta(hours=24)
    # Получаем детали, которые не приняты и которые созданы более 24 часов назад
    orders = Order.objects.filter(is_ready=False,
                                  created__lte=safe_line).annotate(num_orders=Count('orderitem'))
    empty_orders = filter(lambda x: x.num_orders == 0, orders)
    for order in empty_orders:
        print(f'Удалён заказ #{order.id}')
        order.delete()
