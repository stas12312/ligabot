import itertools
import os
from datetime import datetime, timedelta
from math import ceil

import pytz
import transliterate
from django.conf import settings
from django.db.models import Q
from django.utils import timezone
from telebot import types

from . import models

ACTION_MODE_PHOTO = 0  # Принятие фото
ACTION_MODE_FILE = 1  # Принятие файла

HELP_CATEGORY = 0  # Перейти в заметку
HELP_NOTE = 1  # Перейти в категорию
ACTION_BACK_TO_NOTES = 10  # Вернутся к заметкам
ACTION_BACK_TO_CATEGORY = 11  # Вернутся к категориям
ACTION_CLOSE = 12  # Закрыть окно

ACTION_APPLY_TASK = 20  # Завершить задачу

TASK_MAIN = 50  # Выбор типа задач
TASK_LIST_DELIVER = 51  # Перейти к списку поставленных задач
TASK_LIST_EXECUTE = 52  # Перейти к списку исполняемых задач
TASK_SHOW_DELIVER = 53  # Показать поставленную задачу
TASK_SHOW_EXECUTE = 54  # Показать исполняемую задачу

ACTION_BACK_TO_TASK = 53  # Возвращение к задаче
ACTION_CLOSE_DELIVER_TASK = 55  # Завершение поставленной задачи
ACTION_CLOSE_EXECUTE_TASK = 56  # Завершение выполняемой задачи
ACTION_REJECT_TASK = 57  # Отправка задачи на доработку
ACTION_TRANSFER_TASK = 58  # Отправка задачи другому исполнителю (Переход на выбор пользователя)
ACTION_CHOICE_USER_FOR_TRANSFER_TASK = 59  # Выбор пользователя, которому передать задачу

ACTION_CHOICE_DEPARTAMENT = 60  # Выбор отдела, для которого передать задачу
ACTION_CHOICE_DEADLINE = 61  # Выбор дедлайна
ACTION_CHOICE_EXECUTE = 62  # Выбор исполнителя
SEND_ALL_DEPARTMENT = 63  # Отправка задачи всему отделу
ACTION_TRANSFER_DEADLINE = 64  # Пренести дедлайн
ACTION_INPUT_REASON = 65  # Ввод причины переноса дедлайна

APPLY_TRANSFER_DEADLINE = 66
CANCEL_TRANSFER_DEADLINE = 67
CUSTOM_DEADLINE = 68

LIST_NEED_APPLY_TASKS = 69  # Показать задачи, которые нужно подтвердить
TASK_NEED_APPLY = 70  # Показать задачу, которые нужно подтвердить
LIST_WAIT_APPLY_TASKS = 71  # Показать задачи, ожидающие подтверждения
TASK_WAIT_APPLY = 72  # Показать задачу, которая ожидает подтверждения

CREATE_DETAIL_ORDER = 73  # Сформировать новый заказ по деталям

MONTH = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
         'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']


def slug(string):
    """
    Функция формирует slug-строку
    :param string: строка для которой формируется slug
    :return: slug-строку
    """
    try:
        return transliterate.translit(string, reversed=True).lower().replace(' ', '_')
    except Exception as e:
        return string.lower().replace(' ', '_')


def get_images(path, time=None):
    """
    Функция возвращает словарь со списком фотографий
    :param path: Путь к папке, где хранятся фото
    :param time: Время начала занятия
    :return: Словарь с фото
    """
    abs_path = os.path.join(
        settings.BASE_DIR,
        path
    )
    if not os.path.exists(abs_path):
        return []
    if time:
        images = {'start': [], 'end': []}  # словарь для фото
        if not os.path.isdir(abs_path):
            return None  # Если директории не существует, возвращаем None
        for item in os.listdir(abs_path):
            # получаем дату создания файла
            date_created = datetime.fromtimestamp(os.path.getmtime(os.path.join(abs_path, item)))

            # Преобразовываем в нужный часовой пояс
            date_created = timezone.localtime(date_created.replace(tzinfo=pytz.utc))
            ctime = datetime.time(date_created)  # получаем время создания файла

            time_min1 = ctime.hour * 60 + ctime.minute
            time_min2 = time.hour * 60 + time.minute
            if time_min1 - time_min2 < 60:
                # images.append(os.path.join(path, item))
                images['start'].append(os.path.join(path, item))
            else:
                images['end'].append(os.path.join(path, item))
    else:
        images = []
        for item in os.listdir(abs_path):
            images.append(os.path.join(path, item))
    return images


def get_weak(date, shift=0):
    """
    Функция возвращает начало и конец недели
    :param date: Дата, для которой находится начало и конец недели
    :param shift: Сдвиг недели
    :return: Начало и конец недели
    """
    date = date - timedelta(days=shift)
    delta = 6 - date.weekday()
    start_date = (date - timedelta(days=date.weekday() - shift))
    end_date = (date + timedelta(days=delta + shift))
    return {'start': start_date, 'end': end_date}


def find_branch(name: str):
    try:
        int(name)
        return models.Branch.objects.get(num_code=name)
    except (ValueError, models.Branch.DoesNotExist):
        list_words = name.split()
        # Если много слов, то не делаем перестановки
        if len(list_words) > 3:
            try:
                return models.Branch.objects.get(name__iexact=name)
            except models.Branch.DoesNotExist:
                pass
        else:
            list_perm = itertools.permutations(list_words)
            for item in list_perm:
                str_name = ' '.join(item)
                try:
                    return models.Branch.objects.get(name__iexact=str_name)
                except models.Branch.DoesNotExist:
                    pass
    return None


def teacher_required(bot, type_='message'):
    """
    Функция декоратор, проверяет, что пользователь авторизован
    :param bot: Объект бот, для отправки сообщения
    :param type_: Тип сообщения
    :return: Функция декоратор
    """

    def decorator(func):
        def wrapped(msg):
            chat_id = 0
            if type_ == 'message':
                chat_id = msg.chat.id
            elif type_ == 'call':
                chat_id = msg.message.chat.id
            try:
                models.Teacher.objects.get(chat_id=chat_id, is_active=True)
                func(msg)
            except models.Teacher.DoesNotExist:
                bot.send_message(chat_id, 'Для выполнения данного действия требуется авторизация')
                return lambda: False

        return wrapped

    return decorator


def get_keyboard(titles: list, resize: bool = True):
    """
    Получение клавиатуры для telegram бота
    :param titles: Список с названиями кнопок
    :param resize: Подстраивание клавиатуры под кол-во кнопок
    :return: Клавиатура
    """
    if not titles:
        return types.ReplyKeyboardRemove()
    markup = types.ReplyKeyboardMarkup(resize_keyboard=resize)
    for title in titles:
        row = []
        # Если внутри список, то формируем строку из вложенного спистка
        if isinstance(title, list):
            for item in title:
                row.append(types.KeyboardButton(item))

        else:
            row.append(types.KeyboardButton(title))
        markup.row(*row)
    return markup


def get_last_actual_branches(datetime_: datetime, teacher):
    """
    Получение актуальных филиалов для выдачи их преподавателю
    :param datetime_: Текущая дата
    :param teacher: Преподаватель
    :return:
    """
    start = datetime_ - timedelta(days=30)  # Месяц назад
    prev_day = datetime_ - timedelta(days=7)  # Неделя назад
    # Получаем  отчёты за последние 30 дней
    logs = models.LogSending.objects.filter(
        teacher=teacher,
        date__gte=start,
        date__lte=datetime_,
    ).order_by('branch__name', '-date')

    # Убираем дубли у филиалов
    logs_branches = logs.distinct('branch__name')
    # Список для результирующих филиалов
    branches = list()

    # Проверяем, какой филиал был неделю назад
    try:
        prev_log = models.LogSending.objects.get(teacher=teacher, date=prev_day.date())
        prev_branch = prev_log.branch
    except models.LogSending.DoesNotExist:
        prev_branch = None

    for i, log in enumerate(logs_branches):
        # Получаем количество приходов на филиал и вычисляем коэффицент
        count = logs.filter(branch=log.branch).count()
        c = i + count
        # Если филиал был в текущий день
        # на прошлой неделе, то увеличиаем
        # его коэффицент
        if log.branch == prev_branch:
            c *= 1.4
        branches.append((log.branch, c))
        print(log.branch, c)
    return list(sorted(branches, key=lambda x: x[1], reverse=True))


def get_branches_on_weak(datetime_: datetime, teacher):
    """
    Получение филиалов за последние 6 дней
    :param datetime_: Дата
    :param teacher: Преподаватель
    :return:
    """
    # Если суббота или воскресенье
    # то нужно получить следующий понедельник
    start = datetime_ - timedelta(days=6)

    # Находим все отчёты за период с субботы по пятницу
    logs = models.LogSending.objects.filter(
        teacher=teacher,
        date__gte=start,
    ).order_by('-date')
    return logs


def get_minutes(time):
    """Получение времени в минутах"""
    return time.hour * 60 + time.minute


def get_help_url():
    """Получение клавиатуры для сообщения о помощи"""
    help_url = [
        ['Инструкция с картинками 1 [КАБИНЕТ]', 'https://ligajournal.ru/instructions/lesson/'],
        ['Инструкция с картинками 2 [ПОДГОТОВКА]', 'https://ligajournal.ru/instructions/prepare/'],
        ['Инструкция с картинками 3 [СОРТИРОВКА]', 'https://ligajournal.ru/instructions/sorting/'],
    ]
    # Если настроена папка для Google диска добавляем инструкцию с папкой
    if settings.DRIVE_FOLDER:
        help_url.append(['Инструкция с картинками 4 [ФОТО С ЗАНЯТИЙ]', 'https://ligajournal.ru/instructions/photo/'])
    return help_url


def get_dashboard_info(datetime_: datetime, is_dynamic: bool):
    """Получение актуальный филиалов для Dashboard"""
    start = datetime_ - timedelta(hours=1)
    start.replace(hour=23, minute=59)

    if start.replace(hour=23, minute=59) > start > start.replace(hour=23, minute=00):
        start = start.replace(hour=00, minute=0)
    end = datetime_ + timedelta(hours=1)

    if is_dynamic:
        # Получаем филиалы, на которых есть занятия
        # интервал +- 1 час
        branches = models.Branch.objects.filter(is_active=True,
                                                lessons__day_of_weak=datetime_.weekday() + 1,
                                                lessons__start__gte=start,
                                                lessons__end__gte=datetime_).order_by('name')
        # Получаем отчеты с занятий с занятями
        # в инетрвале +- 1 час
        query = Q(date=datetime_) \
                & ((Q(lesson__start__gte=start) & Q(lesson__end__gte=datetime_))
                   | (Q(time__gte=start) & Q(time__lte=end)))

        logs = models.LogSending.objects.filter(query)
    else:
        branches = models.Branch.objects.filter(is_active=True)
        logs = models.LogSending.objects.filter(date=datetime_)
    return branches, logs


def get_main_menu_keyboard():
    """Получение клавиатуры главного меню"""
    titles = ['✉ Отправить отчёт', ['🆘', '❓ Справка', '📜 Прочее'],
              ['🔔 Новая задача', '📝 Список задач']]

    return get_keyboard(titles)


def get_action_keyboard(mode):
    """Получение клавиатуры выбора действия с фото"""
    if mode == ACTION_MODE_FILE:
        titles = ['📷 Фото с занятий']
    elif mode == ACTION_MODE_PHOTO:
        titles = [['🖼 Фото кабинета'], ['📦 Сортировка', '📝 Подготовка']]
        if settings.DRIVE_FOLDER:
            titles.append('📷 Фото с занятий')
    else:
        titles = []

    titles.append('❌ Отмена загрузки')
    return get_keyboard(titles)


def get_help_category_keyboard():
    """Получение справочного сообщение"""
    # Получаем категории
    categories = models.HelpCategory.objects.all()
    keyboard = types.InlineKeyboardMarkup()
    for category in categories:
        # Формируем callback_data category:{id_category}
        data = f'{HELP_CATEGORY}:{category.id}'
        keyboard.add(types.InlineKeyboardButton(text=f'{category.name}', callback_data=data))
    keyboard.add(types.InlineKeyboardButton(text='❌ Закрыть', callback_data=f'{ACTION_CLOSE}'))
    return keyboard


def get_help_notes_keyboard(notes):
    """Получение клавиатуры выбора заметки"""
    keyboard = types.InlineKeyboardMarkup()
    for note in notes:
        # Формируем callback_data category:{id_category}
        data = f'{HELP_NOTE}:{note.id}'
        keyboard.add(types.InlineKeyboardButton(text=f'{note.title}', callback_data=data))
    keyboard.add(types.InlineKeyboardButton(text='⬅️ Назад', callback_data=f'{ACTION_BACK_TO_CATEGORY}'))
    return keyboard


def get_list_task_keyboard(tasks, mode):
    """
    Формирование клавиатуры для списка задач
    :param tasks: Список задач, для которых формируется клавиатура
    :param mode: Тип задач
    :return:
    """
    keyboard = types.InlineKeyboardMarkup()
    now = timezone.localtime(timezone.now())  # Получаем текущее время
    for task in tasks:
        text = clear_text(task.text)
        # Если есть дедлайн, то получаем разицу в днях
        # для того, чтобы добавить пометки к задачам
        if task.deadline:
            delta = datetime.date(task.deadline) - datetime.date(now)
            # Если задача просрочена
            if delta.days < 0:
                text = '🔥 ' + text
            # Если дедлайн сегодня
            if delta.days == 0:
                text = '⏰ ' + text
        keyboard.add(types.InlineKeyboardButton(
            text=text,
            callback_data=f'{mode}:{task.id}'
        ))
    keyboard.add(types.InlineKeyboardButton(
        text='⬅️ Назад',
        callback_data=f'{TASK_MAIN}'
    ))
    return keyboard


def get_type_task_keyboard(teacher):
    """
    Получение клавиатуры для выбора типа задач
    Выполняемые - задачи, которые выполняем пользователь
    Поставленные - задачи, поставленные пользователем
    :param teacher: Пользователь, для которого формируется клавиатура
    :return: Клавиатуру
    """
    keyboard = types.InlineKeyboardMarkup()
    # Получаем задачи, которые выполняет пользователь
    execute_tasks_count = models.Task.objects.filter(executor=teacher, status=1).count()
    if execute_tasks_count:
        keyboard.add(types.InlineKeyboardButton(
            text=f'Выполняемые ({execute_tasks_count})', callback_data=f'{TASK_LIST_EXECUTE}'))

    # Получаем поставленные задачи
    deliver_tasks_count = models.Task.objects.filter(
        director=teacher, status__in=[0, 1]).exclude(executor=teacher).count()
    if deliver_tasks_count:
        keyboard.add(types.InlineKeyboardButton(
            text=f'Поставленные ({deliver_tasks_count})', callback_data=f'{TASK_LIST_DELIVER}'))

    # Получаем кол-во задач, которые мы должны подтвердить
    wait_deliver_tasks_count = models.Task.objects.filter(director=teacher, status=2).count()
    if wait_deliver_tasks_count:
        keyboard.add(types.InlineKeyboardButton(
            text=f'Нужно подтвердить ({wait_deliver_tasks_count})', callback_data=f'{LIST_NEED_APPLY_TASKS}'))

    # Получаем кол-во задач, которые ожидают подтверждения со стороны заказчика
    wait_execute_tasks_count = models.Task.objects.filter(executor=teacher, status=2).count()
    if wait_execute_tasks_count:
        keyboard.add(types.InlineKeyboardButton(
            text=f'Ожидают подтверждения ({wait_execute_tasks_count})', callback_data=f'{LIST_WAIT_APPLY_TASKS}'
        ))
    keyboard.add(types.InlineKeyboardButton(text='💻 Посмотреть в браузере',
                                            url=f'{settings.ALLOWED_HOSTS[0]}/profile/{teacher.password}/tasks/'))
    keyboard.add(types.InlineKeyboardButton(text='❌ Закрыть', callback_data=f'{ACTION_CLOSE}'))
    return keyboard


def get_departament_keyboard(teacher):
    """Получение клавиатуры для выбора отдела"""
    departments = models.Departament.objects.all()
    # Если сотрудник не офисные, то выбераем департаменты для преподавателей
    if not teacher.office:
        departments = departments.filter(only_office=False)

    keyboard = types.InlineKeyboardMarkup()
    departments = list_to_table(departments)
    for departament in departments:
        buttons_row = []
        for row in departament:
            buttons_row.append(get_inline_button(row.name, [ACTION_CHOICE_DEPARTAMENT, row.id]))

        keyboard.row(*buttons_row)

    if teacher.office:
        # Кнопка поставить задачу себе для офиса
        keyboard.add(get_inline_button('📌 Поставить себе', [ACTION_CHOICE_EXECUTE, -1, teacher.id]))
    # Кнопка отмены
    keyboard.add(get_inline_button('❌ Отмена', [ACTION_CLOSE]))
    return keyboard


def get_user_link(user):
    """Получение ссылки на пользователя"""
    return f'[{user.full_name}](tg://user?id={user.chat_id})'


def get_inline_button(text, params):
    """Удобная обертка для получение inline кнопки"""
    return types.InlineKeyboardButton(
        text=text,
        callback_data=':'.join(map(str, params))
    )


def get_inline_keyboard(buttons=()):
    """Формирование inline клавиатуры"""
    keyboard = types.InlineKeyboardMarkup()
    for button in buttons:
        if isinstance(button, list):
            keyboard.row(*[get_inline_button(title, mode) for title, mode in button])
        else:
            keyboard.add(get_inline_button(button[0], button[1]))
    return keyboard


def get_deadlines_keyboard(task_id=-1):
    """Формирование клавиатуры для выбора дедлайна"""
    # Получаем список вех дедлайнов
    deadlines = models.Deadlines.objects.all()
    keyboard = types.InlineKeyboardMarkup()
    buttons = []
    for deadline in deadlines:
        buttons.append(get_inline_button(deadline.label, [ACTION_CHOICE_DEADLINE, deadline.days, task_id]))
    buttons.append(get_inline_button('❎ Без дедлайна', [ACTION_CHOICE_DEADLINE, -1, task_id]))

    # Конкретный дедлайн можно установить только при создании задачи
    buttons.append(get_inline_button('🖋 Свой', [CUSTOM_DEADLINE, task_id]))
    button_rows = list_to_table(buttons, 2)
    button_rows.append([get_inline_button('❌ Отмена', [ACTION_CLOSE])])

    for row in button_rows:
        keyboard.row(*row)
    return keyboard


def list_to_table(list_, col=2):
    """Преобразования списка в думерный список"""
    row = ceil(len(list_) / col)
    print(row)
    new_list = []
    for i in range(row):
        new_list.append(list_[i * col:i * col + col])
    return new_list


def clear_text(text):
    text = text.replace('[', r'\[')
    text = text.replace('_', r'\_')
    text = text.replace('*', r'\*')
    return text
