import json
import re
from datetime import timedelta

from django.conf import settings as global_settings
from django.contrib.admin.views.decorators import staff_member_required
from django.core.paginator import Paginator
from django.db.models import Sum, Q
from django.http import HttpResponseNotFound, HttpResponseRedirect, HttpResponse
from django.http import JsonResponse
from django.shortcuts import redirect
from django.shortcuts import render, get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from .bot import mailing_bot, delete_messages
from .forms import *
from .models import *


@staff_member_required
def teachers_list(request):
    teachers = Teacher.objects.filter(is_active=True)
    return render(
        request,
        'telegrambot/teachers.html',
        context={'teachers': teachers}
    )


@staff_member_required
def lessons_list(request):
    lessons = Lesson.objects.all()
    return render(
        request,
        'telegrambot/lessons.html',
        context={'lessons': lessons}
    )


@staff_member_required
def branches_list(request):
    branches = Branch.objects.filter(is_active=True)
    return render(
        request,
        'telegrambot/branches.html',
        context={'branches': branches}
    )


@staff_member_required
def logs_list(request):
    form = LogFilterForm(request.GET)
    logs = LogSending.objects.all().order_by('-date', '-time')
    lessons = Lesson.objects.all()
    #  фильтр
    if form.is_valid():
        if form.cleaned_data['min_date']:
            logs = logs.filter(date__gte=form.cleaned_data['min_date'])
        if form.cleaned_data['max_date']:
            logs = logs.filter(date__lte=form.cleaned_data['max_date'])
        if form.cleaned_data['min_time'] or form.cleaned_data['min_time'] == 0:
            logs = logs.filter(time_before__gte=form.cleaned_data['min_time'])
        if form.cleaned_data['max_time'] or form.cleaned_data['max_time'] == 0:
            logs = logs.filter(time_before__lte=form.cleaned_data['max_time'])
        if form.cleaned_data['lesson']:
            logs = logs.filter(lesson__name=form.cleaned_data['lesson'])
        if form.cleaned_data['branch']:
            logs = logs.filter(branch=form.cleaned_data['branch'])
    # количество записей на странице
    paginator = Paginator(logs, 30)
    # получаем из запроса номер страницы
    page_number = request.GET.get('page', 1)
    page = paginator.get_page(page_number)
    context = {
        'page': page,
        'lessons': lessons,
        'form': form,
    }

    return render(request, 'telegrambot/logs.html', context=context)


@staff_member_required
def log_detail(request, id):
    log = LogSending.objects.get(id=id)
    if log.lesson:
        log_time = log.lesson.start
        have_lesson = True
    else:
        log_time = None
        have_lesson = False
    images = utils.get_images(log.photos, log_time)
    return render(
        request,
        'telegrambot/log_detail.html',
        context={'log': log, 'images': images, 'flag': have_lesson}
    )


@staff_member_required
def teacher_detail(request, id):
    teacher = Teacher.objects.get(id=id)
    logs = LogSending.objects.filter(
        teacher__exact=id).order_by('-date', '-time')
    return render(
        request,
        'telegrambot/teacher_detail.html',
        context={'teacher': teacher, 'logs': logs}
    )


@staff_member_required
def branch_detail(request, id):
    branch = Branch.objects.get(id=id)
    logs = LogSending.objects.filter(branch=id).order_by('-date', '-time')
    num_page = request.GET.get('page', 1)
    paginator = Paginator(logs, 20)
    page = paginator.get_page(num_page)
    return render(
        request,
        'telegrambot/branch_detail.html',
        context={'branch': branch, 'page': page}
    )


@staff_member_required
@csrf_exempt
def search(request):
    if request.method == "GET":
        branches = []
        search_text = request.GET.get('search_text', '')
        if search_text:
            search_text = search_text.title().strip()
            teachers = Teacher.objects.filter(
                full_name__icontains=search_text
            )[:5]
            branches = Branch.objects.filter(name__icontains=search_text)[:5]
        else:
            teachers = []

        return render(request,
                      'telegrambot/ajax_search.html',
                      {'teachers': teachers, 'branches': branches}
                      )
    else:
        return HttpResponseNotFound()


@staff_member_required
def get_updates(request):
    count_from_request = int(request.GET.get('count', ' '))
    now = timezone.localtime(timezone.now())
    branches, logs = utils.get_dashboard_info(now, Settings.get_value('dynamic_dashboard'))
    log_count = logs.count()
    count = str(logs.count()) + str(branches.count())

    if int(count) != int(count_from_request):
        # n = log_count - count
        # Если кол-во записей изменилось, то формируем ответ, со всеми логами
        teacher_count = Teacher.objects.filter(is_active=True).count()
        branch_count = Branch.objects.filter(is_active=True).count()
        lesson_count = Lesson.objects.all().count()

        late_count = logs.filter(time_before__lte=0).count()

        branches_info = {}
        for branch in branches:
            branches_info[branch.id] = {'name': branch.name, 'logs': []}

        for log in logs:
            log_info = {
                'id': log.id,
                'teacher': log.teacher.get_short_name,
                'lesson': log.lesson.__str__() if log.lesson else 'Нет информации',
                'time': log.time.strftime('%H:%M'),
                'time_before': log.time_before if log.time_before else False
            }
            # Если для лога нет филиала, добавляем соответсвующий ключ в словарь
            if log.branch.id not in branches_info:
                branches_info[log.branch.id] = {'name': log.branch.name, 'logs': []}
            # Добавляем лог для филиала
            branches_info[log.branch.id]['logs'].append(log_info)

        return JsonResponse(
            {'is_update': True, 'count': log_count, 'late_count': late_count, 'updates': branches_info})
    return JsonResponse({'is_update': False})


@staff_member_required
def dashboard(request):
    now = timezone.localtime(timezone.now())
    teacher_count = Teacher.objects.filter(is_active=True).count()
    branch_count = Branch.objects.filter(is_active=True).count()
    lesson_count = Lesson.objects.all().count()

    branches, logs = utils.get_dashboard_info(now, Settings.get_value('dynamic_dashboard'))
    print(branches)
    # Словарь 'Филиал': ['Отчёты']
    branches_logs = {}
    for branch in branches:
        branches_logs[branch] = []

    for log in logs:
        branch = log.branch
        if branch not in branches:
            branches_logs[branch] = []
        branches_logs[branch].append(log)

    late_count = logs.filter(time_before__lte=0).count()
    context = {
        'teacher_count': teacher_count,
        'branch_count': branch_count,
        'lesson_count': lesson_count,
        'branches': branches,
        'today_log': logs,
        'late_count': late_count,
        'branches_logs': branches_logs,
    }
    return render(request, 'telegrambot/dashboard.html', context=context)


@staff_member_required
def prepare_list(request):
    form = DateFilterForm(request.GET)
    date_ = datetime.date(timezone.localtime(timezone.now()).today())

    if form.is_valid():
        if form.cleaned_data['date']:
            date_ = form.cleaned_data['date']

    # Получаем начало и конец недели от сегоднешней даты
    weak = utils.get_weak(date_)
    prepares = Preparation.objects.filter(date__gte=weak['start'], date__lte=weak['end'])
    teachers = Teacher.objects.filter(is_active=True).extra(where=["additional & 4 = 0"])

    # количество записей на странице
    paginator = Paginator(teachers, 30)
    # получаем из запроса номер страницы
    page_number = request.GET.get('page', 1)
    page = paginator.get_page(page_number)

    teachers = page.object_list
    # формируем результат
    result = {}
    for teacher in teachers:
        if prepares.filter(teacher=teacher).count():
            p = prepares.get(teacher=teacher)
            images = utils.get_images(p.photos)
            result[teacher] = (p, images, p.id)
        else:
            result[teacher] = False

    prev_weak = date_ - timedelta(days=7)
    next_weak = None
    now_weak_ = None
    now_weak = utils.get_weak(datetime.date(timezone.localtime(timezone.now())))

    # Проверяем, есть ли собрание на этой недели
    # try:
    #    meeting = Meeting.objects.get(create_datetime__gte=weak['start'], create_datetime__lte=weak['end'])
    # except Meeting.DoesNotExist:
    #    meeting = None

    # Если запрашиваемая неделя меньше текущей
    if weak['start'] < now_weak['start']:
        next_weak = date_ + timedelta(days=7)
        next_weak = next_weak.strftime('%d.%m.%Y')
        now_weak_ = timezone.localtime(timezone.now()).strftime('%d.%m.%Y')

    context = {
        'date': {'start': weak['start'].strftime('%d.%m.%Y'),
                 'end': weak['end'].strftime('%d.%m.%Y'),
                 'get': date_.strftime('%d.%m.%Y'),
                 'prev_weak': prev_weak.strftime('%d.%m.%Y'),
                 'next_weak': next_weak,
                 'now_weak': now_weak_
                 },

        'result': result,
        'form': form,
        'page': page,
        'meeting': None,
    }
    return render(request, 'telegrambot/prepare_list.html', context=context)


@staff_member_required
def prepare_detail(request, id):
    context = {}
    return render(request, 'telegrambot/prepare_detail.html', context=context)


def handle_uploaded_file(f):
    import secrets
    file_name = secrets.token_hex(16) + '.' + f.name.split('.')[-1]
    name = os.path.join(global_settings.BASE_DIR,
                        global_settings.MEDIA_ROOT,
                        file_name)

    with open(name, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
    return file_name


@staff_member_required
@csrf_exempt
def mailing(request):
    form = MailingForm(request.POST, request.FILES)
    print(request.FILES)
    if form.is_valid():
        title = ''
        if form.cleaned_data['title']:
            title = form.cleaned_data['title']
        body = form.cleaned_data['body']
        teacher_list = form.cleaned_data['teachers']
        date_mail = form.cleaned_data['date']
        time_mail = form.cleaned_data['time']
        if date_mail and time_mail:
            datetime_mail = timezone.localtime(timezone.now()).combine(date_mail, time_mail)
            recivers = []
            for teacher in teacher_list:
                recivers.append(teacher.id)

            json_resivers = json.dumps(recivers)
            message = Message(title=title,
                              body=body,
                              receivers=0,
                              detail=json_resivers,
                              is_sender=False,
                              datetime=datetime_mail)
            message.save()
            form = MailingForm()
        else:
            image_link = ''
            if form.cleaned_data['file']:
                image = handle_uploaded_file(request.FILES['file'])
                image_link = f'[​​​​​​​​​​​]({global_settings.ALLOWED_HOSTS[0]}{os.path.join(global_settings.MEDIA_URL, image)})'
            results = mailing_bot(title, image_link + body, teacher_list)  # отправляем сообщения
            if len(results[0]['success']) == 1:
                res = results[0]['success'][0]
            else:
                res = len(results[0]['success'])
            # добавляем сообщение в БД
            detail = json.dumps(results[1])
            Message.objects.create(
                title=title,
                body=body,
                receivers=res,
                detail=detail,
                datetime=timezone.localtime(timezone.now())
            )
            return render(request, 'telegrambot/success_mailing.html',
                          context={'success': results[0]['success'], 'errors': results[0]['errors']})
    # Формирования списков для выбора нужных категорий
    all_ = Teacher.objects.filter(is_active=True)
    teachers = all_.filter(teacher=True)
    office = all_.filter(office=True)

    messages_list = Message.objects.all()
    context = {'form': form,
               'messages': messages_list,
               'all': all_,
               'teachers': teachers,
               'office': office,
               }
    return render(request, 'telegrambot/mailing.html', context=context)


def instruction(request):
    """Отображение страницы с инструкцией"""
    #  Добавить динамически генерируемую инструкцию
    return render(request, 'telegrambot/instruction.html')


def instruction_prepare(request):
    return render(request, 'telegrambot/prepare_instruction.html')


@staff_member_required
def settings(request):
    settings_ = Settings.objects.all()  # Получаем все настройки из БД
    for setting in settings_:
        setting.value = setting.value.replace('\n', '<br>')  # Заменяем перенос строки на html перенос строки
    context = {
        'settings': settings_,
    }
    return render(request, 'telegrambot/settings.html', context=context)


@staff_member_required
def apply_settings(request):
    # Получаем значения из POST запроса
    type_ = request.POST.get('type', '')  # Возможно лишний параметр
    value = request.POST.get('value', '')
    name = request.POST.get('name', '')
    #  Проверяем, что есть все значения
    if type_ and value and name:
        try:
            stg = Settings.objects.get(name=name)
        except Settings.DoesNotExist:
            return JsonResponse({'status': 'ERROR', 'msg': 'Настройка не найдена'})

        if stg.type == 0:
            if int(value) > 1 or int(value) < 0:
                return JsonResponse({'status': 'ERROR', 'msg': 'Некорректное булевое значение'})
        elif stg.type == 1:
            try:
                int(value)
            except ValueError:
                return JsonResponse({'status': 'ERROR', 'msg': 'Некорректное численное значение'})
        else:
            # получаем значение настройки и заменяем html символы
            value = value.replace('&nbsp;', ' ').replace('<br>', '\n')

            # подготавливаем регулярное выражение для поиска параметров
            template = r'\{(.*?)\}'
            reg = re.compile(template, re.S)
            received_params = reg.findall(value)  # извлекаем все параметры параметры

            value = re.compile(r'<span.*?>|</span>').sub('', value)  # вырезаем html тег

            # параметры в БД указывается через ','
            avaliable_params = stg.parametrs.replace(' ', '').split(',')  # доступные параметры

            for param in received_params:
                # проверяем есть ли принятые парметры в БД
                if param not in avaliable_params:
                    return JsonResponse({'status': 'ERROR', 'msg': 'Не найден параметр {{{}}}'.format(param)})
            # Сохраняем значение
        stg.value = value
        stg.save()
        return JsonResponse({'status': 'OK', 'msg': '✓Настройки сохранены'})
    else:
        return JsonResponse({'status': 'ERROR', 'msg': 'Нет всех данных'})


@staff_member_required
def delete_message(request, id):
    mailing_info = Message.objects.get(id=id)
    is_delete_message = request.GET.get('delete_message', '')

    # Если есть информация об отправленных сообщениях, то удаляем их
    if mailing_info.detail and mailing_info.is_sender:
        if is_delete_message:
            messages_info = json.loads(mailing_info.detail)
            delete_messages(messages_info)
    mailing_info.delete()
    return redirect('mailing')


@staff_member_required
def history_of_sorting(request):
    action = request.POST.get('action', '')
    sort_id = request.POST.get('id', '')
    mode = request.GET.get('mode', '')

    if action and sort_id:
        sort_id = int(sort_id)
        sort = Sort.objects.get(id=sort_id)
        if action == 'accept':
            sort.status = 1
            mailing_bot(
                'Уведомление', 'Ваша сортировка c филиала {} от {} принята'.format(
                    sort.branch, sort.date.strftime('%d.%m.%Y')), [sort.teacher])
        elif action == 'unaccept':
            comment = request.POST.get('comment', '')
            if comment:
                mailing_bot(
                    'Уведомление', 'Сортировка с филиала {} от {} не принята\nПричина: {}'.format(
                        sort.branch,
                        sort.date.strftime('%d.%m.%Y'),
                        comment), [sort.teacher], )
                sort.status = 2
                sort.comment = comment
        sort.save()
        return redirect('/sorting?mode={}'.format(mode), )

    if mode == 'all':
        sorting = Sort.objects.all()
    elif mode == 'accepted':
        sorting = Sort.objects.filter(status=1)
    else:
        sorting = Sort.objects.filter(status=0)
    paginator = Paginator(sorting, 10)
    num_page = request.GET.get('page', 1)
    page = paginator.get_page(num_page)
    context = {'sorting': sorting,
               'page': page}
    return render(request, 'telegrambot/sorting_history.html', context=context)


@staff_member_required
def get_bot_log(request):
    path = os.path.join(global_settings.STATIC_ROOT, 'bot.log')
    f = open(path)
    text = f.read().splitlines()[:-200:-1]
    context = {
        'title': 'История запросов к боту',
        'text': text
    }
    return render(request, 'telegrambot/bot_log.html', context=context)


@staff_member_required
def add_meeting(request):
    data = {}
    if request.method == 'GET':
        date_ = request.GET.get('date', '')
        if date_:
            date_ = timezone.localtime(timezone.now()).strptime(date_, '%d.%m.%Y')
        else:
            date_ = timezone.localtime(timezone.now())
        try:
            weak = utils.get_weak(date_)
            Meeting.objects.get(create_datetime__gte=weak['start'], create_datetime__lte=weak['end'])
        except Meeting.DoesNotExist:
            Meeting.objects.create(create_datetime=date_)
        return HttpResponseRedirect(f'/preparations?date={date_.strftime("%d.%m.%Y")}')
    return JsonResponse({'status': 'ERROR'})


@staff_member_required
def delete_meeting(request, id):
    try:
        Meeting.objects.get(id=id).delete()
        return redirect('/preparations?status=OK')
    except Meeting.DoesNotExist:
        return redirect('/preparations/status=ERROR')


@staff_member_required
def show_meeting(request, id):
    meeting = Meeting.objects.get(id=id)
    teachers = meeting.teachers.all()
    ctx = {
        'meeting': meeting,
        'teachers': teachers,
    }
    return render(request, 'telegrambot/show_meeting.html', context=ctx)


@staff_member_required
def list_meeting(request):
    pass


@staff_member_required
def edit_meeting(request, id):
    if request.method == 'GET':
        action = request.GET.get('action', '')  # Определяем действие
        id_meeting = id
        if not action:
            return HttpResponseNotFound()
        meeting = Meeting.objects.get(id=id_meeting)

        if action == 'add_teacher':
            teacher_id = request.GET.get('teacher_id', '')
            meeting.teachers.add(teacher_id)

        elif action == 'add_users':
            pass
        elif action == 'remove_teacher':
            teacher_id = request.GET.get('teacher_id', '')
            teacher = meeting.teachers.get(id=teacher_id)
            meeting.teachers.remove(teacher)

    return HttpResponse('OK')


def delete_image(request):
    if request.method == 'GET':
        object_ = request.GET.get('object', '')
        path = request.GET.get('path', '')
        id = request.GET.get('id', '')
        info = {}
        if object_ == 'prepare':
            obj_ = Preparation.objects.get(id=id)
            dir_path = obj_.photos
            info['object'] = 'Подготовка'
        elif object_ == 'sorting':
            obj_ = Sort.objects.get(id=id)
            dir_path = obj_.path
            info['object'] = 'Сортировка'
        else:
            return HttpResponseNotFound()

        # Если абослютный путь, то делаем его относительным
        if path[0] == os.path.sep:
            path = path[1:]

        # Абсолютный путь для фото
        abs_path = os.path.join(global_settings.BASE_DIR, path)

        # Удаляем фото
        os.remove(abs_path)
        info['text'] = 'Фото успешно удалено'

        # Проверка на удаление всех фото
        if len(os.listdir(dir_path)) == 0:
            info['is_delete'] = True
            obj_.delete()
        else:
            info['is_delete'] = False

        return JsonResponse({'status': 'OK', 'info': info})
    return JsonResponse({'status': 'ERROR'})


@csrf_exempt
def photo_callback(request, chat_id, path):
    json_data = json.loads(request.body)
    shutil.rmtree(os.path.join(global_settings.BASE_DIR, path))

    # Извлекаем данные из запроса
    date = datetime.strptime(json_data['date'].strip(), '%d.%m.%Y')
    branch = Branch.objects.get(name=json_data['branch'])
    teacher = Teacher.objects.get(chat_id=chat_id)
    path = json_data['url']

    photo = PhotoLog.objects.get(teacher=teacher, photo_created=date, branch=branch)
    photo.path = path
    photo.save()
    # Экранируем символы, которые могут привести к ошибке
    # и затем отправляем сообщение преподавателю
    message = utils.clear_text(json_data['message'])
    mailing_bot('', message, [teacher])
    return HttpResponse('ok')


@staff_member_required
def photo_list(request):
    form = DateFilterForm(request.GET)

    date_ = datetime.date(timezone.localtime(timezone.now()).today())
    if form.is_valid():
        if form.cleaned_data['date']:
            date_ = form.cleaned_data['date']

    weak = utils.get_weak(date_, -2)

    photos = PhotoLog.objects.filter(photo_created__gte=weak['start'], photo_created__lte=weak['end']) \
        .order_by('-created')

    photo_count = photos.aggregate(Sum('photo_count'))['photo_count__sum']
    teacher_count = len(set(photos.values_list('teacher', flat=True)))

    # количество записей на странице
    paginator = Paginator(photos, 30)
    # получаем из запроса номер страницы
    page_number = request.GET.get('page', 1)
    page = paginator.get_page(page_number)

    result = page.object_list

    prev_weak = date_ - timedelta(days=7)
    next_weak = None
    now_weak_ = None
    now_weak = utils.get_weak(datetime.date(timezone.localtime(timezone.now())), -2)

    # Если запрашиваемая неделя меньше текущей
    if weak['start'] < now_weak['start']:
        next_weak = date_ + timedelta(days=7)
        next_weak = next_weak.strftime('%d.%m.%Y')
        now_weak_ = timezone.localtime(timezone.now()).strftime('%d.%m.%Y')

    context = {
        'date': {'start': weak['start'].strftime('%d.%m.%Y'),
                 'end': weak['end'].strftime('%d.%m.%Y'),
                 'get': date_.strftime('%d.%m.%Y'),
                 'prev_weak': prev_weak.strftime('%d.%m.%Y'),
                 'next_weak': next_weak,
                 'now_weak': now_weak_
                 },

        'result': result,
        'form': form,
        'page': page,
        'meeting': None,
        'photo_count': photo_count,
        'teacher_count': teacher_count,
    }
    return render(request, 'telegrambot/photo_list.html', context=context)


@staff_member_required
def reference(request):
    """Получение страницы с категориями"""
    return render(request, 'telegrambot/reference.html')


@staff_member_required
def tasks_list(request):
    """
    Получение страницы с поставленными задачами
    :param request: Входящий запрос
    :return:
    """
    form = TaskFilterForm(request.GET)
    tasks = Task.objects.exclude(type=2)
    now = timezone.localtime(timezone.now())

    # Применяем фильтры
    if form.is_valid():
        print(form.cleaned_data)
        if form.cleaned_data['min_date']:
            tasks = tasks.filter(created__gte=form.cleaned_data['min_date'])
        if form.cleaned_data['max_date']:
            tasks = tasks.filter(created__lte=form.cleaned_data['max_date'])
        if form.cleaned_data['departament']:
            tasks = tasks.filter(departament__name=form.cleaned_data['departament'])
        if form.cleaned_data['is_overdue']:
            tasks = tasks.filter(deadline__date__lt=now, status__in=[1, 2])
        if form.cleaned_data['status']:
            tasks = tasks.filter(status=form.cleaned_data['status'])
        if form.cleaned_data['type']:
            tasks = tasks.filter(type=form.cleaned_data['type'])
        if form.cleaned_data['sort_field']:
            sign = form.cleaned_data['sorting_order']
            tasks = tasks.order_by(sign + form.cleaned_data['sort_field'])

    # количество записей на странице
    paginator = Paginator(tasks, 50)
    # получаем из запроса номер страницы
    page_number = request.GET.get('page', 1)
    page = paginator.get_page(page_number)

    context = {'form': form,
               'page': page,
               }
    return render(request, 'telegrambot/tasks_list.html', context=context)


def task_detail(request, task_id):
    """
    Вывод подробной информации о задаче
    :param request: Входящий запрос
    :param task_id: Идентификатор задачи
    :return:
    """
    # Получаем задачу по id
    task = get_object_or_404(Task, pk=task_id)

    # Получаем история для задачи
    task_history = TaskHistory.objects.filter(task=task)
    context = {
        'task': task,
        'task_history': task_history

    }
    return render(request, 'telegrambot/task_detail.html', context=context)


def profile_logs(request, user_token):
    """Показ логов для преподавателя"""
    try:
        teacher = Teacher.objects.get(password=user_token, is_active=True)
    except Teacher.DoesNotExist:
        return HttpResponseNotFound('Преподаватель не найден')
    import locale
    locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')
    now = timezone.localtime(timezone.now())

    print(f'Пользователь {teacher.full_name} зашёл на страницу истории отчётов')
    # Получаем начало месяца
    start = now - timedelta(days=now.day - 1)
    form = DateIntervalForm(request.GET)
    query = Q(teacher=teacher)
    min_date = start
    max_date = now
    # Название месяца, если не передан интервал
    month = ''
    if form.is_valid():
        if not form.cleaned_data['min_date'] and not form.cleaned_data['max_date']:
            month = utils.MONTH[now.month - 1]
            query = query & Q(date__gte=start)
        else:
            if form.cleaned_data['min_date']:
                query = query & Q(date__gte=form.cleaned_data['min_date'])
                min_date = form.cleaned_data['min_date']
            else:
                query = query & Q(date__gte=start)
            if form.cleaned_data['max_date']:
                query = query & Q(date__lte=form.cleaned_data['max_date'])
                max_date = form.cleaned_data['max_date']

    # Получаем отчеты преподавателя
    logs = LogSending.objects.filter(query)
    sort = Sort.objects.filter(query)
    prepares = Preparation.objects.filter(query)
    photos = PhotoLog.objects.filter(teacher=teacher, created__gte=min_date, created__lte=max_date)
    orders = Order.objects.filter(teacher=teacher, created__gte=min_date, created__lte=max_date)

    context = {
        'teacher': teacher,
        'logs': logs,
        'sorts': sort,
        'prepares': prepares,
        'photos': photos,
        'orders': orders,
        'form': form,
        'user_token': user_token,
        'month': month,
    }
    return render(request, 'telegrambot/profile_logs.html', context=context)


def profile_tasks(request, user_token):
    """Показ задач для пользователей"""
    try:
        user = Teacher.objects.get(password=user_token, is_active=True)
    except Teacher.DoesNotExist:
        return HttpResponseNotFound('Преподаватель не найден')
    print(f'Пользователь {user.full_name} зашёл на страницу списка задач')
    # Получаем активные задачи, где пользователь поставщик
    deliver_tasks = Task.objects.filter(director=user).exclude(status__in=[-1, 3]).exclude(executor=user)
    for task in deliver_tasks:
        task.text = task.text.replace('\n', '<br>')
    # Получаем активные задача, где пользователь исполнитель
    execute_tasks = Task.objects.filter(executor=user).exclude(status__in=[-1, 3])
    for task in execute_tasks:
        task.text = task.text.replace('\n', '<br>')

    now = timezone.localtime(timezone.now())
    start = now - timedelta(days=30)
    # Получаем закрытые задачи, где мы либо исполнитель, либо заказчик
    closed_tasks = Task.objects.filter(
        Q(executor=user) |
        Q(director=user)
    ).filter(status=3, closed__gte=start).order_by('-created')

    # Получам отделы пользователя
    departments = []
    for role in user.roles.all():
        departments.append(role.departament)

    # Получаем задачи для отделов
    department_tasks = Task.objects.filter(departament__in=departments).exclude(closed__lte=start)

    context = {
        'user': user,
        'deliver_tasks': deliver_tasks,
        'execute_tasks': execute_tasks,
        'closed_tasks': closed_tasks,
        'department_tasks': department_tasks,
    }

    return render(request, 'telegrambot/profile_tasks.html', context=context)


@csrf_exempt
def order_callback(request, order_token):
    order = get_object_or_404(Order, token=order_token)
    data = json.loads(request.body, encoding='UTF-8')
    # Создаем пункт для общего заказа
    OrderItem.objects.create(
        order=order,
        set_number=data['set_number'],
        set_type=data['set_name'],
        list_details=data['list_detail'],
    )

    return HttpResponse(order_token)


@staff_member_required
def order_list(request):
    """Просмотр списка заказов"""
    orders = Order.objects.filter(is_ready=False)
    context = {
        'orders': orders,
        'is_ready': False,
    }
    return render(request, 'telegrambot/orders_list.html', context=context)


@staff_member_required
def order_list_archive(request):
    """Просмотр списка выполненных заказов"""
    orders = Order.objects.filter(is_ready=True)
    context = {
        'orders': orders,
        'is_ready': True,
    }
    return render(request, 'telegrambot/orders_list.html', context=context)


@staff_member_required
def order_detail(request, order_id):
    """Просмотр конкретного заказа"""
    # Получаем заказ
    order = get_object_or_404(Order, pk=order_id)

    # Если пришёл POST запрос, заказ собран
    if request.method == 'POST':
        print(request.POST)
        comment = request.POST.get('comment', None)
        print(comment)
        # Формируем сообщение о завершении заказа преподавателю
        order.is_ready = True

        message = f'Ваш заказ для филиала {order.branch} от ' \
                  f'{order.created.strftime("%d.%m.%Y")} собран\n'

        if comment:
            message += f'Комментарий: {comment}'
            order.comment = comment

        order.save()
        mailing_bot('Уведомление', message, [order.teacher])
        return redirect('orders_list')

    # Получаем пункты заказа(наборы)
    items = OrderItem.objects.filter(order=order)

    # Считаем общий список недостающих деталей
    results = {}
    for item in items:
        # Для всех деталей в списке
        for detail in item.list_details:
            if detail['id'] in results:
                results[detail['id']]['need_count'] += detail['need_count']
            else:
                results[detail['id']] = {'name': detail['name'],
                                         'need_count': detail['need_count'],
                                         'image': detail['image']}
    context = {
        'order': order,
        'items': items,
        'details': results
    }
    return render(request, 'telegrambot/order_detail.html', context=context)


def order_detail_token(request, order_token):
    order = get_object_or_404(Order, token=order_token)

    # Получаем пункты заказа(наборы)
    items = OrderItem.objects.filter(order=order)
    results = {}

    # Считаем общий список недостающих деталей
    for item in items:
        # Для всех деталей в списке
        for detail in item.list_details:
            if detail['id'] in results:
                results[detail['id']]['need_count'] += detail['need_count']
            else:
                results[detail['id']] = {'name': detail['name'],
                                         'need_count': detail['need_count'],
                                         'image': detail['image']}
    context = {
        'order': order,
        'items': items,
        'details': results
    }
    return render(request, 'telegrambot/order_detail_token.html', context=context)


@staff_member_required
def static_sorting_by_branch(request):
    form = DateFilterForm(request.GET)
    now = timezone.localtime(timezone.now())

    start = now - timedelta(days=now.day - 1)
    form = DateIntervalForm(request.GET)
    min_date = start
    max_date = now
    if form.is_valid():
        if not form.cleaned_data['min_date'] and not form.cleaned_data['max_date']:
            month = utils.MONTH[now.month - 1]
        else:
            if form.cleaned_data['min_date']:
                min_date = form.cleaned_data['min_date']
            if form.cleaned_data['max_date']:
                max_date = form.cleaned_data['max_date']
    result = {}
    # Получаем сортировки за период
    sort = Sort.objects.filter(date__lte=max_date, date__gte=min_date)

    # Получаем все активные филиалы
    branches = Branch.objects.filter(is_active=True)
    for branch in branches:
        result[branch] = sort.filter(branch=branch)

    result = dict(sorted(result.items(), key=lambda kv: len(kv[1]), reverse=True))

    context = {
        'form': form,
        'result': result,
        'min_date': min_date,
        'max_date': max_date,
    }

    return render(request, 'telegrambot/static_sorting_by_branch.html', context=context)


@staff_member_required
def static_sorting_by_teacher(request):
    date_form = DateIntervalForm(request.GET)
    count_form = NumberIntervalForm(request.GET)

    now = timezone.localtime(timezone.now())

    short_flag = request.GET.get('short_flag', False)

    start = now - timedelta(days=now.day - 1)
    min_date = start
    max_date = now
    if date_form.is_valid():
        if not date_form.cleaned_data['min_date'] and not date_form.cleaned_data['max_date']:
            month = utils.MONTH[now.month - 1]
        else:
            if date_form.cleaned_data['min_date']:
                min_date = date_form.cleaned_data['min_date']
            if date_form.cleaned_data['max_date']:
                max_date = date_form.cleaned_data['max_date']

    result = {}
    # Получаем сортировки за период
    sort = Sort.objects.filter(date__lte=max_date, date__gte=min_date)
    # При кратком выводе, оставляем только принятые наборы
    if short_flag:
        sort = sort.filter(status=1)

    # Если режим краткого вывода и POST-запрос
    if short_flag and request.method == 'POST':
        receivers = Teacher.objects.filter(id__in=request.POST.getlist('receivers'))
        text = request.POST.get('text')
        mailing_bot('Уведомлние', text, receivers)

    # Для всех преподавателей формируем итоговую статистику по сортировкам
    for teacher in Teacher.objects.filter(is_active=True, teacher=True):
        result[teacher] = sort.filter(teacher=teacher)

    result = dict(sorted(result.items(), key=lambda kv: len(kv[1]), reverse=True))
    # Фильтр на количество сортировок
    min_value = 0
    max_value = 99999999
    if count_form.is_valid():
        if not count_form.cleaned_data['min_value'] is None:
            min_value = count_form.cleaned_data['min_value']
        if not count_form.cleaned_data['max_value'] is None:
            max_value = count_form.cleaned_data['max_value']

    result = {k: v for k, v in result.items() if max_value >= len(v) >= min_value}
    context = {
        'date_form': date_form,
        'count_form': count_form,
        'result': result,
        'min_date': min_date,
        'max_date': max_date,
        'short_flag': short_flag,
    }

    return render(request, 'telegrambot/static_sorting_by_teacher.html', context=context)


@staff_member_required
def error_detail(request, error_id):
    error_log = get_object_or_404(BotErrorLog, id=error_id)
    try:
        request_ = json.loads(error_log.request)
        request_ = json.dumps(request_, indent=4, ensure_ascii=False)
        error_log.request = request_.replace('\n', '<br>')
    except Exception as e:
        pass
    context = {
        'error_log': error_log
    }
    return render(request, 'telegrambot/error_detail.html', context=context)


@staff_member_required
def errors_list(request):
    errors = BotErrorLog.objects.all()
    context = {
        'errors': errors,
    }
    return render(request, 'telegrambot/errors_list.html', context=context)
