from django.contrib import admin

from .models import *


# Register your models here.


@admin.register(Teacher)
class TeacherAdmin(admin.ModelAdmin):
    list_display = ('id', 'full_name', 'office', 'teacher', 'is_active', 'password', 'status', 'chat_id', 'additional')
    list_display_links = ('id', 'full_name')
    search_fields = ('full_name',)


@admin.register(LogSending)
class LogSendingAdmin(admin.ModelAdmin):
    list_display = ('id', 'date', 'time', 'time_before', 'teacher', 'branch', 'lesson')


@admin.register(Lesson)
class Lesson(admin.ModelAdmin):
    list_display = ('id',  'day_of_weak', 'name', 'start', 'end')
    list_display_links = ('id', 'name')


@admin.register(NewUser)
class NewUserAdmin(admin.ModelAdmin):
    list_display = ('id', 'chat_id', 'status')
    list_display_links = ('id', 'chat_id')


@admin.register(Preparation)
class PreparationAdmin(admin.ModelAdmin):
    list_display = ('id', 'teacher', 'date', 'time', 'photos')
    list_display_links = ('id', 'teacher')


@admin.register(TempData)
class TempDataAdmin(admin.ModelAdmin):
    list_display = ('id', 'teacher', 'date', 'time', 'branch', 'lesson', 'counter')
    list_display_links = ('id', 'teacher')


@admin.register(Branch)
class AdminBranch(admin.ModelAdmin):
    list_display = ('num_code', 'name', 'is_active', 'get_lessons')
    list_display_links = ('name', 'num_code')


@admin.register(Message)
class AdminMessage(admin.ModelAdmin):
    list_display = ('title', 'body', 'datetime', 'receivers', 'detail', 'is_sender')
    list_display_links = ('title', 'body')


@admin.register(Settings)
class AdminSettings(admin.ModelAdmin):
    list_display = ('id', 'name', 'description', 'type', 'value')
    list_display_links = ('id', 'name')


@admin.register(Sort)
class AdminSort(admin.ModelAdmin):
    list_display = ('id', 'teacher', 'branch', 'date', 'time', 'count', 'status', 'comment', 'path')


@admin.register(Meeting)
class AdminMeeting(admin.ModelAdmin):
    list_display = ('id', 'create_datetime')


@admin.register(PhotoLog)
class AdminPhotoLog(admin.ModelAdmin):
    list_display = ('teacher', 'created', 'branch', 'photo_created', 'photo_count')


@admin.register(Task)
class AdminTask(admin.ModelAdmin):
    list_display = ('id', 'director', 'executor', 'branch', 'text',
                    'status', 'created', 'closed', 'departament', 'deadline', 'type')
    list_filter = ('status', 'departament', 'type')
    list_display_links = ('id', 'text')
    date_hierarchy = 'created'


@admin.register(HelpNotes)
class AdminHelpNotes(admin.ModelAdmin):
    list_display = ('title', 'category', 'text', 'updated')
    list_filter = ('category',)


@admin.register(BotErrorLog)
class AdminBotErrorLog(admin.ModelAdmin):
    list_display = ('created', 'info', 'user')


admin.site.register(HelpCategory)
admin.site.register(Role)
admin.site.register(Departament)
admin.site.register(Deadlines)
admin.site.register(TaskHistory)
admin.site.register(Order)
admin.site.register(OrderItem)
