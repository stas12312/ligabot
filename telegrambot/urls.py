from django.urls import path

from . import views, bot

urlpatterns = [
    path('settings', views.settings, name='settings'),
    path('apply-settings', views.apply_settings, name='apply-settings'),
    path('instruction', views.instruction, name='instruction'),
    path('instruction/prepare', views.instruction_prepare, name='instrucion_prepare'),
    path('logs', views.logs_list, name='logs_list'),
    path('mailing/delete/<int:id>', views.delete_message, name='delete_mailing'),
    path('mailing', views.mailing, name='mailing'),
    path('logs/get_updates', views.get_updates, name='get_updates'),
    path('preparations', views.prepare_list, name='prepare_list'),
    path('preparations<int:id>', views.prepare_detail, name='prepare_detail'),
    path('teachers/<int:id>', views.teacher_detail, name='teacher_detail'),
    path('logs/<int:id>', views.log_detail, name='log_detail'),
    path('teachers', views.teachers_list, name='teachers_list'),
    path('lessons', views.lessons_list, name='lessons_list'),
    path('branches', views.branches_list, name='branches_list'),
    path('branches/<int:id>', views.branch_detail, name='branch_detail'),
    path('telegrambot/<bot_token>', bot.reception_update, name='recive_update'),
    path('search', views.search, name='search'),
    path('', views.dashboard, name='dashboard'),

    path('sorting/', views.history_of_sorting, name='history_of_sorting'),
    path('sorting/statistic/branches/', views.static_sorting_by_branch, name='static_sorting_by_branch'),
    path('sorting/statistic/teachers/', views.static_sorting_by_teacher, name='static_sorting_by_teacher'),

    path('logs/botlog', views.get_bot_log, name='bot_log'),
    path('logs/errors/', views.errors_list, name='errors_list'),
    path('logs/errors/<int:error_id>/', views.error_detail, name='error_log'),
    # URL для собраний
    path('utils/delete-image/', views.delete_image, name='delete_image'),

    path('photo_callback/<int:chat_id>/<path:path>', views.photo_callback, name='photo_callback'),
    path('order_callback/<str:order_token>/', views.order_callback, name='order_callback'),

    path('photos/', views.photo_list, name='photo_list'),

    path('reference/', views.reference, name='reference'),

    path('tasks/', views.tasks_list, name='tasks_list'),
    path('tasks/<int:task_id>', views.task_detail, name='task_detail'),

    path('profile/<user_token>/logs/', views.profile_logs, name='profile_logs'),
    path('profile/<user_token>/tasks/', views.profile_tasks, name='profile_tasks'),


    path('orders/', views.order_list, name='orders_list'),
    path('orders/archive/', views.order_list_archive, name='orders_list_archive'),
    path('orders/<int:order_id>/', views.order_detail, name='order_detail'),
    path('orders/<str:order_token>/', views.order_detail_token, name='order_detail_token'),
]
