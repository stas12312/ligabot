import os
import secrets
import shutil
from datetime import datetime

from django.conf import settings
from django.contrib.postgres import fields
from django.contrib.postgres.fields import JSONField
from django.db import models
from django.shortcuts import reverse
from django.utils import timezone

from . import utils


class Departament(models.Model):
    """Модель отделов"""
    name = models.CharField(max_length=64, verbose_name='Название')
    only_office = models.BooleanField(verbose_name='Только для офиса', default=False)

    class Meta:
        verbose_name = 'Отдел'
        verbose_name_plural = 'Отделы'

    def __str__(self):
        return self.name


class Role(models.Model):
    """Модель роли"""
    name = models.CharField(max_length=64, verbose_name='Название')
    departament = models.ForeignKey(Departament, verbose_name='Отдел', on_delete=models.PROTECT, null=True, blank=True)
    danger_notification = models.BooleanField(verbose_name='Оповещение тревожной кнопкой', default=False)

    class Meta:
        verbose_name = 'Должность'
        verbose_name_plural = 'Должности'

    def __str__(self):
        return self.name


class Teacher(models.Model):
    """Модель преподавателя"""
    full_name = models.CharField(max_length=150, db_index=True, verbose_name='ФИО', unique=True)
    slug_name = models.CharField(max_length=150, null=True, blank=True)
    chat_id = models.IntegerField(null=True, default=0)  # chat_id в телеграмме
    status = models.IntegerField(null=True, default=0, verbose_name='Статус')   # Статус в системе
    password = models.CharField(max_length=128, null=True, blank=True, verbose_name='Токен')  # Пароль в системе
    additional = models.IntegerField(verbose_name='Дополнительное поле', default=0)
    is_active = models.BooleanField(verbose_name='Активный', default=True)
    roles = models.ManyToManyField(Role, blank=True, verbose_name='Роли')
    office = models.BooleanField(verbose_name='Офисный сотрудник', default=False)
    teacher = models.BooleanField(verbose_name='Преподаватель', default=True)

    def save(self, *args, **kwargs):
        if not self.slug_name:
            self.slug_name = utils.slug(self.full_name)
        if not self.password:
            self.password = secrets.token_urlsafe(64)
        super(Teacher, self).save(*args, **kwargs)

    def __str__(self):
        return '{}'.format(self.full_name)

    def get_absolute_url(self):
        return reverse('teacher_detail', kwargs={'id': self.id})

    @property
    def get_short_name(self):
        full_name = self.full_name.split(' ')
        last_name = full_name[0]
        initial_1 = full_name[1][0] + '.' if len(full_name) >= 2 else ''
        initial_2 = full_name[2][0] + '.' if len(full_name) >= 3 else ''

        return '{} {} {}'.format(
            last_name,
            initial_1,
            initial_2)

    class Meta:
        ordering = ['-is_active', "full_name"]
        verbose_name = 'Пользователь бота'
        verbose_name_plural = 'Пользователи бота'


class NewUser(models.Model):
    """Модель незарегистрированного пользователя"""
    chat_id = models.IntegerField()
    status = models.IntegerField(default=0)

    class Meta:
        verbose_name = 'Неавторизованный пользователь'
        verbose_name_plural = 'Неавторизованые пользователи'


class Lesson(models.Model):
    DAY_OF_WEAK = (
        (1, 'ПН'),
        (2, 'ВТ'),
        (3, 'СР'),
        (4, 'ЧТ'),
        (5, 'ПТ'),
        (6, 'СБ'),
        (7, 'ВС'),
    )

    """Модель занятия"""
    name = models.CharField(verbose_name='Название', max_length=100, db_index=True)
    start = models.TimeField(verbose_name='Начало занятия')
    end = models.TimeField(verbose_name='Конец занятия')
    day_of_weak = models.IntegerField(
        verbose_name='День недели',
        choices=DAY_OF_WEAK
    )

    def __str__(self):
        return '{} - {}({}-{})'.format(
            self.get_day_of_weak_display(),
            self.name,
            self.start.strftime('%H:%M'),
            self.end.strftime('%H:%M'))

    class Meta:
        ordering = ['day_of_weak', 'start']
        verbose_name = 'Занятие'
        verbose_name_plural = 'Занятия'


class Branch(models.Model):
    """Модель филиала"""
    name = models.CharField(max_length=256, db_index=True)
    slug_name = models.CharField(max_length=256, blank=True)
    lessons = models.ManyToManyField(Lesson, verbose_name='Занятия', blank=True)
    is_active = models.BooleanField(default=True, verbose_name='Действующий')
    num_code = models.IntegerField(default=0, verbose_name='Код филиала')

    def save(self, *args, **kwargs):
        self.slug_name = utils.slug(self.name)
        # Создание папки филиала
        path = os.path.join(
            settings.BASE_DIR,
            settings.STATIC_DIRS,
            settings.IMAGE_DIR,
            self.slug_name
        )
        try:
            os.mkdir(path)
        except OSError:
            pass
        super(Branch, self).save(*args, **kwargs)

    # Вывод списка занятий
    def get_lessons(self):
        lessons_list = self.lessons.all()
        lessons_str = ''
        for lesson in lessons_list:
            lessons_str += ', ' + lesson.__str__()
        return lessons_str.lstrip(', ')

    get_lessons.short_description = 'Занятия'

    def __str__(self):
        return '{}'.format(self.name)

    def get_absolute_url(self):
        return reverse('branch_detail', kwargs={'id': self.id})

    @property
    def get_count_log(self):
        return LogSending.objects.filter(
            date=datetime.now(),
            branch=self
        ).count()

    @property
    def is_have_logs(self):
        count = LogSending.objects.filter(date=timezone.localtime(timezone.now()), branch=self).count()

        if count:
            return True
        else:
            return False

    @property
    def get_logs(self):
        logs = LogSending.objects.filter(date=timezone.localtime(timezone.now()), branch=self)
        return logs

    class Meta:
        ordering = ['-is_active', 'name']
        verbose_name = 'Филиал'
        verbose_name_plural = 'Филиалы'


day = ['ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ', 'ВС']


class LogSending(models.Model):
    """Модель для ведения логов отправки фото"""
    date = models.DateField(verbose_name='Дата')
    time = models.TimeField(verbose_name='Время прибытия')
    time_before = models.IntegerField(verbose_name='Время до начала занятия', null=True, blank=True)
    teacher = models.ForeignKey(Teacher, on_delete=models.SET_NULL, verbose_name='Преподаватель', null=True)
    branch = models.ForeignKey(Branch, on_delete=models.SET_NULL, null=True, verbose_name='Филиал')
    lesson = models.ForeignKey(Lesson, on_delete=models.SET_NULL, verbose_name='Занятие', blank=True, null=True)
    photos = models.TextField(null=True, default='None', verbose_name='Местоположение фото')
    # end_time = models.TimeField(verbose_name='Время ухода', null=True)

    def get_absolute_url(self):
        return reverse('log_detail', kwargs={'id': self.id})

    @property
    def is_today(self):
        return timezone.localtime(timezone.now()).today() == self.date

    def save(self, *args, **kwargs):
        if self.lesson:
            self.time_before = (self.lesson.start.hour - self.time.hour) * 60 \
                               + int(self.lesson.start.minute - self.time.minute)
        super(LogSending, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        path = os.path.join(settings.BASE_DIR, self.photos)
        shutil.rmtree(path, ignore_errors=True)
        super(LogSending, self).delete(*args, **kwargs)

    class Meta:
        ordering = ['-id']
        verbose_name = 'Лог'
        verbose_name_plural = 'Логи'


class Preparation(models.Model):
    """Модель для подготовки"""
    date = models.DateField(verbose_name='Дата')
    time = models.TimeField(verbose_name='Время')
    photos = models.TextField(max_length=100, verbose_name='Местоположение фото')
    teacher = models.ForeignKey(Teacher, on_delete=models.PROTECT, verbose_name='Преподаватель')

    def __str__(self):
        return "Подготовка {} {}".format(self.teacher, self.date)

    class Meta:
        ordering = ['-date']  # по умолчанию сортировка по дате
        verbose_name = 'Подготовка'
        verbose_name_plural = 'Подготовки'


class TempData(models.Model):
    """Модель для хранения временных данных"""
    date = models.DateField(verbose_name='Дата')
    time = models.TimeField(verbose_name='Время')
    teacher = models.ForeignKey(Teacher, on_delete=models.PROTECT, verbose_name='Преподаватель')
    branch = models.ForeignKey(Branch, on_delete=models.SET_NULL, blank=True, null=True, verbose_name='Филиал')
    lesson = models.ForeignKey(Lesson, on_delete=models.SET_NULL, blank=True, null=True, verbose_name='Занятие')
    counter = models.IntegerField(blank=True, null=True, default=0, verbose_name='Счетчик')

    def __str__(self):
        return "{} {} {} {}".format(self.id, self.teacher, self.date, self.time.strftime('%H:%M'))

    def delete(self, *args, **kwargs):
        path = os.path.join(settings.BASE_DIR, settings.MEDIA_DIR,
                            settings.TEMP_DIR, self.teacher.slug_name)
        shutil.rmtree(path, ignore_errors=True)
        self.teacher.status = 0
        self.teacher.save()
        super(TempData, self).delete(*args, **kwargs)

    class Meta:
        ordering = ['-date']
        verbose_name = 'Временная запись'
        verbose_name_plural = 'Временные записи'


class Message(models.Model):
    title = models.CharField(max_length=40, verbose_name='Заголовок', blank=True, null=True)
    body = models.TextField(verbose_name='Текст')
    datetime = models.DateTimeField(auto_created=True)
    receivers = models.TextField(verbose_name='Получатели')
    detail = models.TextField(verbose_name='Информация об отправленных сообщений', blank=True, null=True)
    is_sender = models.BooleanField(verbose_name='Отправлено', default=True)

    def __str__(self):
        return '{}'.format(self.title)

    class Meta:
        ordering = ['-datetime']
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'


class Settings(models.Model):
    types = (
        (0, 'Boolean'),
        (1, 'Number'),
        (2, 'String'),
    )

    name = models.CharField(max_length=64, verbose_name='Название')
    description = models.TextField(max_length=256, verbose_name='Описание')
    type = models.IntegerField(choices=types, verbose_name='Тип')
    value = models.TextField(default=0, verbose_name='Значение')
    parametrs = models.TextField(default='', verbose_name='Параметры', blank=True, null=True)

    class Meta:
        verbose_name = 'Настройка'
        verbose_name_plural = 'Настройки'
        ordering = ['type', 'name']

    @staticmethod
    def get_value(name):
        """
        Функция возвращает значение настройки по ее названию в зависимости от её типа
        :param name: Имя настройки
        :return: Значение настройки, если настройка не найдена None
        """
        try:
            setting = Settings.objects.get(name=name)
            if setting.type == 0:
                value = bool(int(setting.value))
            elif setting.type == 1:
                value = int(setting.value)
            elif setting.type == 2:
                value = str(setting.value)
            else:
                value = ''
            return value

        except Settings.DoesNotExist:
            return None


class Sort(models.Model):
    STATUSES = (
        (0, 'На проверке'),
        (1, 'Принята'),
        (2, 'Не принята')
    )

    teacher = models.ForeignKey(Teacher, verbose_name='Преподаватель', null=True, on_delete=models.SET_NULL)
    branch = models.ForeignKey(Branch, verbose_name='Филиал', null=True, on_delete=models.SET_NULL)
    count = models.IntegerField(default=0, blank=True, null=True, verbose_name='Количество')
    date = models.DateField(null=True, verbose_name='Дата сортировки')
    time = models.TimeField(null=True, verbose_name='Время сортировки')
    # 0 - Ожидает проверки, 1 - проверен, 2 - не принят
    status = models.IntegerField(default=0, verbose_name='Статус', choices=STATUSES)
    path = models.TextField(null=True, blank=True, verbose_name='Расположение фото')
    comment = models.CharField(max_length=256, blank=True, verbose_name='Комментарий', default='-')

    class Meta:
        verbose_name = 'Сортировка'
        verbose_name_plural = 'Сортировки'
        ordering = ['-date', '-time']

    def get_photo_links(self):
        return utils.get_images(self.path)

    def delete(self, *args, **kwargs):
        path = os.path.join(settings.BASE_DIR, self.path)
        shutil.rmtree(path, ignore_errors=True)
        super(Sort, self).delete(*args, **kwargs)


class Meeting(models.Model):
    teachers = models.ManyToManyField(Teacher, verbose_name='Преподаватели')
    create_datetime = models.DateTimeField(verbose_name='Дата создания', default=0)

    class Meta:
        verbose_name = 'Собрание'
        verbose_name_plural = 'Собрания'
        ordering = ['-create_datetime']


class PhotoLog(models.Model):
    """ Модель для фото с занятий"""
    teacher = models.ForeignKey(Teacher, verbose_name='Преподаватель', on_delete=models.PROTECT)
    created = models.DateTimeField(auto_now_add=True, verbose_name='Дата отправки')
    branch = models.ForeignKey(Branch, verbose_name='Филиал', on_delete=models.PROTECT)
    photo_created = models.DateField(verbose_name='Дата фотографирования')
    path = models.URLField(verbose_name='Ссылка на папку с фото', default='')
    photo_count = models.IntegerField(verbose_name='Количество фотографий', default=0)

    class Meta:
        verbose_name = 'Фотография с занятия'
        verbose_name_plural = 'Фотографии с занятий'
        ordering = ['-photo_created']


class HelpCategory(models.Model):
    """Модель категорий для справочника"""
    name = models.CharField(max_length=64, verbose_name='Название')

    class Meta:
        verbose_name = 'Категории справочника'
        verbose_name_plural = 'Категории справочника'

    def __str__(self):
        return self.name


class HelpNotes(models.Model):
    """Заметки для справочника"""
    category = models.ForeignKey(HelpCategory, verbose_name='Категория',on_delete=models.PROTECT)
    title = models.CharField(max_length=64, verbose_name='Заголовок')
    text = models.TextField(verbose_name='Содержание')
    updated = models.DateTimeField(auto_now=True, verbose_name='Обновлено')

    class Meta:
        verbose_name = 'Заметка справочника'
        verbose_name_plural = 'Заметки справочника'

    def __str__(self):
        return self.title


class Task(models.Model):
    """Модель для задач"""
    STATUSES = (
        (-1, 'Заполняется'),
        (0, 'Создана'),
        (1, 'Выполняется'),
        (2, 'Ожидает подтверждения'),
        (3, 'Завершена'),
    )

    TYPES = (
        (0, 'Офис'),
        (1, 'Секция'),
        (2, 'Личная'),
    )
    director = models.ForeignKey(Teacher, verbose_name='Постановщик', on_delete=models.PROTECT, related_name='director')
    executor = models.ForeignKey(Teacher, verbose_name='Исполнитель',
                                 on_delete=models.PROTECT, related_name='executor',
                                 null=True, blank=True, default=None)
    text = models.TextField(verbose_name='Описание')
    status = models.IntegerField(choices=STATUSES, verbose_name='Статус')
    created = models.DateTimeField(verbose_name='Создана')
    closed = models.DateTimeField(verbose_name='Завершена', null=True, blank=True, default=None)
    branch = models.ForeignKey(Branch, verbose_name='Филиал', on_delete=models.PROTECT, blank=True, null=True)
    departament = models.ForeignKey(Departament, verbose_name='Ответственный отдел', on_delete=models.PROTECT,
                                    blank=True, null=True)
    deadline = models.DateTimeField(verbose_name='Крайний срок', null=True, blank=True)
    type = models.IntegerField(verbose_name='Тип задачи', choices=TYPES, default=1)
    comment = models.CharField(max_length=512, verbose_name='Комментарий', blank=True, null=True)

    class Meta:
        verbose_name = 'Задача'
        verbose_name_plural = 'Задачи'
        ordering = ['-created']

    def __str__(self):
        return f'{self.branch}: {self.text}'

    @property
    def deadline_left(self):
        now = timezone.localtime(timezone.now())
        if self.deadline:
            delta = self.deadline.date() - now.date()
            return delta.days
        return False


class Deadlines(models.Model):
    """Модель для дедлайнов"""
    days = models.IntegerField(verbose_name='Кол-во дней')
    label = models.CharField(max_length=64, verbose_name='Псевдоним')

    class Meta:
        verbose_name = 'Дедлайн'
        verbose_name_plural = 'Дедлайны'

    def __str__(self):
        return f'{self.label} ({self.days})'


class TaskHistory(models.Model):
    """Модель для истории изменения задачи"""

    task = models.ForeignKey(Task, verbose_name='Задача', on_delete=models.CASCADE)
    comment = models.CharField(max_length=256, verbose_name='Действие')
    created = models.DateTimeField(verbose_name='Дата действия')
    need_action = models.BooleanField(verbose_name='Ожидает действия', default=False)
    info = models.CharField(max_length=256, verbose_name='Служебная информация', blank=True, null=True)
    
    class Meta:
        verbose_name = 'История задач'
        verbose_name_plural = 'Истории задач'
        
    def __str__(self):
        return f'{self.created.strftime("%d.%m.%Y")} - {self.comment}'


class Order(models.Model):
    """Модель заказа деталей"""

    created = models.DateTimeField(verbose_name='Создан')
    token = models.CharField(max_length=128, verbose_name='Токен заказа', blank=True)
    teacher = models.ForeignKey(Teacher, verbose_name='Преподаватель', on_delete=models.PROTECT)
    branch = models.ForeignKey(Branch, verbose_name='Филиал', blank=True, null=True, on_delete=models.PROTECT)
    comment = models.CharField(max_length=512, verbose_name='Комментарий', blank=True, null=True)
    is_ready = models.BooleanField(verbose_name='Собран', default=False)

    class Meta:
        verbose_name = 'Заказ деталей'
        verbose_name_plural = 'Заказы деталей'
        ordering = ['-created']

    def save(self, *args, **kwargs):
        if not self.token:
            self.token = secrets.token_hex(32)
        super(Order, self).save(*args, **kwargs)


class OrderItem(models.Model):
    """Модель заказа на конкретный набор"""

    order = models.ForeignKey(Order, verbose_name='Заказ', on_delete=models.PROTECT)
    set_number = models.IntegerField(verbose_name='Номер набора')
    set_type = models.CharField(max_length=64, verbose_name='Набор')
    list_details = JSONField(verbose_name='Список деталей')

    class Meta:
        verbose_name = 'Заказ на набор'
        verbose_name_plural = 'Заказы на наборы'


class BotErrorLog(models.Model):
    """Модель для хранения информации об ошибках в боте"""
    created = models.DateTimeField(verbose_name='Время создания')
    info = fields.JSONField(verbose_name='Информация об ошибке')
    request = models.TextField(verbose_name='Запрос пользователя')
    user = fields.JSONField(verbose_name='Информация о пользователе')

    class Meta:
        verbose_name = 'Ошибка в боте'
        verbose_name_plural = 'Ошибки в боте'
        ordering = ['-created']
