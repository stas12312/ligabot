from django import forms

from .models import Lesson, Branch, Teacher, Departament


class LogFilterForm(forms.Form):
    min_date = forms.DateField(label='С', required=False, input_formats=['%d.%m.%Y'])
    max_date = forms.DateField(label='До', required=False, input_formats=['%d.%m.%Y'])
    min_time = forms.IntegerField(label='От', required=False)
    max_time = forms.IntegerField(label='До', required=False)
    lessons = set(Lesson.objects.values_list('name', flat=True))
    list_choices = [['', 'Все']]
    for lesson in sorted(lessons, reverse=True):
        list_choices.append([lesson, 'Занятие: {}'.format(lesson)])
    lesson = forms.ChoiceField(choices=list_choices,
                               label='Занятие', required=False,
                               widget=forms.Select(attrs={'class': 'col-12  form-control form-control-sm'}))

    branch = forms.ModelChoiceField(queryset=Branch.objects.all(), label='Филиал', required=False, empty_label='Все',
                                    widget=forms.Select(attrs={'class': 'col-12  form-control form-control-sm'}))


class TaskFilterForm(forms.Form):
    min_date = forms.DateField(label='От', required=False, input_formats=['%d.%m.%Y'])
    max_date = forms.DateField(label='До', required=False, input_formats=['%d.%m.%Y'])
    departments = set(Departament.objects.values_list('name', flat=True))
    departments_list = [('', 'Все')]
    for departament in departments:
        departments_list.append((departament, departament))
    departament = forms.ChoiceField(choices=departments_list, label='Отдел', required=False,
                                    widget=forms.Select(attrs={'class': 'col-12  form-control form-control-sm'}))
    is_overdue = forms.BooleanField(label='Просроченные', required=False)
    STATUSES = (
        ('', 'Все'),
        (1, 'Выполняется'),
        (2, 'Ожидает подтверждения'),
        (3, 'Завершена'),

    )
    status = forms.ChoiceField(choices=STATUSES, label='Статус', required=False,
                               widget=forms.Select(attrs={'class': 'col-12  form-control form-control-sm'}))
    TYPES = (
        ('', 'Все'),
        (0, 'Офис'),
        (1, 'Секция'),
    )

    type = forms.ChoiceField(choices=TYPES,
                             label='Тип', required=False,
                             widget=forms.Select(attrs={'class': 'col-12  form-control form-control-sm'}))

    # Поля для сортировки
    fields = (
        ('', '-'),
        ('deadline', 'Дедлайн'),
    )

    sort_field = forms.ChoiceField(choices=fields, label='Поле сортировки', required=False,
                                   widget=forms.Select(attrs={'class': 'col-12  form-control form-control-sm'}))

    # Порядок сортировки
    sorting_orders = (
        ('', 'По возрасатнию'),
        ('-', 'По убыванию'),
    )
    sorting_order = forms.ChoiceField(choices=sorting_orders, label='Порядок сортировки', required=False,
                                      widget=forms.Select(attrs={'class': 'col-12  form-control form-control-sm'}))


class DateFilterForm(forms.Form):
    date = forms.DateField(label='Дата', required=False, input_formats=['%d.%m.%Y'])


class MailingForm(forms.Form):
    title = forms.CharField(max_length=30, label='Заголовок', required=False, widget=forms.TextInput)
    body = forms.CharField(max_length=5000, label='Сообщение', required=True, widget=forms.Textarea)
    teachers = forms.ModelMultipleChoiceField(queryset=Teacher.objects.filter(chat_id__gt=0, is_active=True),
                                              required=True,
                                              widget=forms.SelectMultiple(attrs={'id': 'jq-milt'})
                                              )
    file = forms.FileField(label='Картинка', required=False)
    date = forms.DateField(label='Дата', required=False, input_formats=['%d.%m.%Y'])
    time = forms.TimeField(label='Время', required=False, input_formats=['%H:%M'])

    def clean(self):
        cleaned_data = super().clean()
        date = cleaned_data.get('date')
        time = cleaned_data.get('time')
        if time or date:
            if not time:
                self.add_error('time', 'Укажите время')
            if not date:
                self.add_error('date', 'Укажите дату')


class DateIntervalForm(forms.Form):
    min_date = forms.DateField(label='От', required=False, input_formats=['%d.%m.%Y'])
    max_date = forms.DateField(label='До', required=False, input_formats=['%d.%m.%Y'])


class NumberIntervalForm(forms.Form):
    min_value = forms.IntegerField(label='От', required=False)
    max_value = forms.IntegerField(label='До', required=False)
