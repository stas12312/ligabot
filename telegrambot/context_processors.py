from django.template.context_processors import request
from .models import Sort
from contextlib import contextmanager
from django.db import transaction
from django.db.transaction import get_connection


def notification(request):
    sort_count = Sort.objects.filter(status=0).count()
    return {
        'count_sort_notification': sort_count
    }


@contextmanager
def lock_table(model):
    with transaction.atomic():
        cursor = get_connection().cursor()
        cursor.execute(f'LOCK TABLE {model._meta.db_table}')
        yield
        cursor.close()
