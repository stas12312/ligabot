# -*- coding: utf-8 -*-

import distutils.dir_util
import json
import logging
import sys
import traceback
from datetime import timedelta

import requests
import telebot
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from telebot import apihelper, types

from .context_processors import lock_table
from .models import *

"""
    teacher.status:
    0 - Ожидание действия
    1 - Ожидание ввода филиала или подготовки
    2 - Ожидание нажатия кнопки
    3 - Ожидание отправки фото и ввода филиала
    10 - Ожидание отправки филиала и даты для фото с занятий
"""
# Папка для хранения временных файлов
TEMP_DIR = os.path.join(settings.BASE_DIR, settings.MEDIA_DIR, settings.TEMP_DIR)

# подключение прокси, для работы с telegram
# apihelper.proxy = {'https': 'socks5://proxy:liga2018@45.32.184.15:1080'}
apihelper.proxy = {'https': 'socks5://v3_112752666:cragARBp@s5.priv.opennetwork.cc:1080'}

# настройка для журнала
LOG_DIR = os.path.join(settings.BASE_DIR, settings.STATIC_DIRS, 'bot.log')
logger = logging.getLogger('log')
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler(LOG_DIR)
fh.setLevel(logging.DEBUG)
logger.addHandler(fh)

# инициализируем бота (отключаем возможность создавать потоки)
bot = telebot.TeleBot(settings.TELEGRAM_BOT_TOKEN, threaded=False)

# Разрешенные форматы изображений
available_mime_types = ['image/jpeg', 'image/jpg', 'image/png']

# Статусы преподавателя в системе
INPUT_PROBLEM_MODE = 100
CHOICE_DEPARTAMENT_MODE = 101
INPUT_REASON_MODE = 102
INPUT_DEADLINE_MODE = 103
INPUT_BRANCH_FOR_ORDER = 104
INPUT_COMMENT_FOR_CLOSE_TASK = 105
INPUT_DANGER_REASON = 106


def save_file(file_info, name_folder, another_path=None):
    """
    Функция сохраняет файл с сервера Telegram во временную папку
    :param file_info: Объёкт в котором хранится информация о файла
    :param name_folder: Папка назначения
    :param another_path: Альтеративный путь для сохранения
    :return: Путь к директории, куда сохранились файлы
    """
    photo = bot.download_file(file_info.file_path)  # скачиваем фото
    # путь типа: директория_сервера/временная_папка/ф_и_о_преподавателя/
    if another_path:
        src = another_path
    else:
        src = os.path.join(TEMP_DIR, name_folder)
    type_file = file_info.file_path.split('.')[-1]
    if not os.path.exists(src):
        os.makedirs(src)

    path = os.path.join(src, file_info.file_id + '.' + type_file)
    with open(path, 'wb+') as new_file:
        new_file.write(photo)

    return path  # возвращаем путь к файлу


def get_lesson(day_of_weak, time, branch, interval=90):
    """
    Функция для получения и сортировки занятий
    :param day_of_weak: Текущий день недели
    :param time: Время
    :param interval: Отклонение в переделах которого ищутся занятия
    :param branch: Филиал для которого ищутся занятия
    :return: Список занятий, попадающий в интервал
    """
    now_min = (time.hour * 60) + int(time.minute)  # текущее время в минутах
    lessons = []
    # получение занятий, которые есть в определенный день
    all_lessons = branch.lessons.filter(day_of_weak=day_of_weak)
    # если занятий нет
    if not all_lessons:
        return False

    for lesson in all_lessons:
        # время начала занятия в минутах
        les_time = (lesson.start.hour * 60) + int(lesson.start.minute)
        # Если занятие попадает в интервал, то добавляем его в результирующий список
        if abs(les_time - now_min) < interval:
            lessons.append(lesson)
    return lessons


def move_files(from_dir, to_dir, delete=False):
    """
    Функция для перемещения файлов между директориями
    :param from_dir: Директория откуда перемещаются файлы
    :param to_dir: Директория куда перемещаются файлы
    :param delete: True - удалить
    :return: None
    """
    # копируем файлы из временной директории в нужную
    distutils.dir_util.copy_tree(from_dir, to_dir)
    if delete:
        # удаляем временную директорию
        shutil.rmtree(from_dir)


def get_keyboard(lessons):
    """
    Функция для формирования клавиатуры выбора занятий
    :param lessons: Список занятий
    :return:
    """
    keyboard = telebot.types.InlineKeyboardMarkup()
    for lesson in lessons:
        button = telebot.types.InlineKeyboardButton(text='{}'.format(lesson), callback_data=lesson.id)
        keyboard.add(button)
    return keyboard


def add_in_log(update, data=None):
    """
    Функция добавляет запрос к боту в лог файл
    :param update: Webhook от Telegram
    :return:
    """
    # Проверяем, что это callback_query
    try:
        callback = update.data
    except AttributeError:
        callback = None

    date_log = timezone.localtime(timezone.now()).strftime("%d.%m.%Y %H:%M")  # текущие дата и время
    name, text = '', ''
    message = update.message
    if not message:
        logger.debug(f'Error:{json.dumps(update)}')
    chat_id = message.chat.id
    link = ''
    size = ''
    callback_data = ''
    if Teacher.objects.filter(chat_id=chat_id):
        name = 'Преподаватель: {}'.format(Teacher.objects.get(chat_id=chat_id).full_name)
    if not callback and message.text:
        text = '[ Текст: {} ]'.format(message.text)
    _type = 'Тип контента: {}'.format(message.content_type)
    if message.content_type == 'photo':
        path = bot.get_file(message.photo[-1].file_id).file_path
        link = '[Ссылка: https://api.telegram.org/file/bot{}/{}]'.format(settings.TELEGRAM_BOT_TOKEN, path)
        size = '[Размер: {0:10.2f} KB]'.format(message.photo[-1].file_size / 1024)
    elif message.content_type == 'sticker':
        path = bot.get_file(message.sticker.file_id).file_path
        link = '[Ссылка: https://api.telegram.org/file/bot{}/{}]'.format(settings.TELEGRAM_BOT_TOKEN, path)
    elif message.content_type == 'document':
        try:
            path = bot.get_file(message.document.file_id).file_path
        except Exception as e:
            path = ''
        link = '[Ссылка: https://api.telegram.org/file/bot{}/{}]'.format(settings.TELEGRAM_BOT_TOKEN, path)
        _type += '({})'.format(message.document.mime_type)
        size = '[Размер: {0:10.2f} MB]'.format(message.document.file_size / 1024 / 1024)
    elif callback:
        callback_data = '[callback_data: {}]'.format(callback)
    if update.message.caption:
        caption = '[Подпись: {}]'.format(update.message.caption)
    else:
        caption = ''
    log = '[{}]: [chat_id: {}][{}] {} [ {} ] {} {} {} {}'.format(
        date_log, chat_id, name, text, _type, caption, link, size, callback_data)
    logger.debug(log)


def mailing_bot(title, body, list_teachers=None):
    """
    Функция для отправки сообщения
    :param title: Заголовок сообщения
    :param body: Текст сообщения
    :param list_teachers: Список получателей
    :return: Кортеж с информацией об отправленных сообщениях
    """
    results = {'success': [], 'errors': []}  # словарь для результатов отправки

    # формируем сообщение
    message = '*{title}*\n{body}'.format(title=title, body=body)
    messages_info = []
    # перебираем список преподавателей
    for teacher in list_teachers:
        try:
            result = bot.send_message(teacher.chat_id, message, parse_mode='Markdown')
            results['success'].append(teacher)
            message_info = {'chat_id': teacher.chat_id, 'message_id': result.message_id}
            messages_info.append(message_info)
        except Exception as e:
            results['errors'].append({'t': teacher, 'e': e})

    return results, messages_info


def save_temp_files(teacher: Teacher, branch: Branch, temp: TempData):
    """Сохраняем фотографии и возвращаем путь"""
    date_ = temp.date

    # вычисляем путь для постоянного хранения
    path = os.path.join(
        settings.MEDIA_DIR,
        settings.IMAGE_DIR,
        branch.slug_name,
        teacher.slug_name,
        date_.strftime('%d-%m-%Y'))

    # абсолютный путь до временной директории
    from_path = os.path.join(
        TEMP_DIR,
        teacher.slug_name)

    # абсолютный путь до постоянной директории
    to_path = os.path.join(
        settings.BASE_DIR,
        path)

    move_files(from_path, to_path, delete=True)
    return path


def send_notification(log):
    """
    Функция отправляем администраторам у которых включены уведомление сообщение о прибытии преподавателя
    :param log: Лог, из которого извлекаются данные о прибытии
    :return:
    """
    # Получаем из БД администраторов с включенными уведомлениями (00000011)
    admins_list = Teacher.objects.extra(where=["additional & 2 = 2 AND additional & 1 = 1"])
    lesson = log.lesson if log.lesson else 'Нет информации'
    # Получаем из БД шаблон уведомления и заполняем его
    msg = Settings.get_value('message_for_notification').format(
        name=log.teacher,
        branch=log.branch,
        lesson=lesson,
        arrival_time=log.time.strftime('%H:%M'),
        time_before_lesson=log.time_before,
    )
    # Отправка уведомления
    mailing_bot('Уведомление', msg, admins_list)


def delete_messages(message_list):
    """
    Удаление отправленных сообщений
    :param message_list: Список сообщений
    :return:
    """
    for message in message_list:
        try:
            bot.delete_message(message['chat_id'], message['message_id'])
        except apihelper.ApiException:
            continue


def send_message_for_admin(title, message):
    """Отправка сообщения администраторам"""
    admins = Teacher.objects.extra(where=["additional & 2 = 2 AND additional & 1 = 1"])

    mailing_bot(title, message, admins)


def send_task_notification(task):
    """Отправка уведомления о постановке задачи нужному отделу"""
    message_text = f'Новая задача от [{task.director}](tg://user?id={task.director.chat_id}):\n{utils.clear_text(task.text)}\n'
    if task.branch:
        message_text = ''.join([message_text, f'Филиал: {task.branch}'])
    receivers = Teacher.objects.filter(
        roles__departament=task.departament).exclude(chat_id__in=[0, task.director.chat_id])
    keyboard = types.InlineKeyboardMarkup()
    keyboard.add(types.InlineKeyboardButton(text='✅ Принять', callback_data=f'{utils.ACTION_APPLY_TASK}:{task.id}'))
    for receiver in receivers:
        bot.send_message(receiver.chat_id, message_text, reply_markup=keyboard, parse_mode="Markdown")


def send_error_for_user(update: types.Update):
    if update.callback_query:
        pass
    else:
        message = update.message
        chat_id = message.chat.id
        bot.send_message(chat_id, 'Возникла ошибка, мы знаем о ней и скоро исправим её:)')


text_before_lesson = """
Зафиксированное время прибытия: {} {}
Ближайшее занятие: 
{},
до начала осталось {} минут
"""

text_after_lesson = """
Зафиксированное время прибытия: {} {}
Ближайшее занятие: 
{},
от начала занятия прошло {} минут
"""

message_for_user = """
Для выполнения данного действия требуется авторизация
"""


@csrf_exempt
def reception_update(request, bot_token):
    """
    Функция принимает WebHook от телеграм и передает запрос на дальнейшую обработку
    :param request: входящий запрос
    :param bot_token: идентификтор бота
    :return Http: ответ сервера
    """
    if bot_token != settings.TELEGRAM_BOT_TOKEN:
        return HttpResponse('Invalid token')

    if (request.META['CONTENT_TYPE'] == 'application/json'
            or request.META['CONTENT_TYPE'] == 'application/x-www-form-urlencoded'):

        json_string = json.loads(request.body.decode())
        update = telebot.types.Update.de_json(json_string)
        # добавляем входящий запрос в логи
        try:
            if not update.callback_query:
                add_in_log(update)
            else:
                add_in_log(update.callback_query)
            # Если бот отключен, то сообщаем об этом
            if not Settings.get_value('active_bot'):
                bot.send_message(update.message.chat.id, 'Проводятся технические работы, бот временно не доступен')
            else:
                bot.process_new_updates([update])
            return HttpResponse(json.dumps({}), content_type="application/json")
        except Exception as e:
            exc_type, exc_value, exc_tb = sys.exc_info()
            try:
                error_log = BotErrorLog()
                error_log.created = timezone.localtime(timezone.now())
                error_info = {
                    'Ошибка': e.__str__(),
                    'Traceback': traceback.format_exception(exc_type, exc_value, exc_tb),
                }
                if update.message:
                    message = update.message
                elif update.callback_query:
                    message = update.callback_query.message
                else:
                    message = None

                if message:
                    user = Teacher.objects.get(chat_id=message.chat.id)
                    user_info = {
                        'ФИО': user.full_name,
                        'Статус': user.status,
                    }

                    error_log.info = error_info
                    error_log.request = request.body.decode()
                    error_log.user = user_info
                    error_log.save()
                    notify_error = f'Возникла ошибка: {e}\n' \
                                   f'Пользователь: {utils.get_user_link(user)}\n' \
                                   f'Время: {error_log.created}\n' \
                                   f'Подробнее: {settings.ALLOWED_HOSTS[0]+reverse("error_log", args=(error_log.pk,))}'
                else:
                    notify_error = 'Неизвестная ошибка'

                bot.send_message(112752666, notify_error, parse_mode='Markdown')
            except apihelper.ApiException as e:
                print(e)
            return HttpResponse(json.dumps({}), content_type="application/json")
    else:
        return HttpResponse("Принятый токен {}".format(bot_token))


# ====================== ОБРАБОТКА НАЖАТОЙ КНОПКИ ============================== #
@bot.callback_query_handler(func=lambda call: True)
@utils.teacher_required(bot=bot, type_='call')
def proc_inline(call: types.CallbackQuery):
    chat_id = call.message.chat.id  # Извлекаем id чата
    message_id = call.message.message_id  # Извлекаем id сообщения
    teacher = Teacher.objects.get(chat_id=chat_id)
    # Извлекаем информацию
    data = call.data
    now = timezone.localtime(timezone.now())
    # Получаем список параметров преобразованных в целое число
    info = [int(param) for param in data.split(':')]
    mode = info[0]
    # Обработка выбора категории
    bot.answer_callback_query(call.id)
    if mode == utils.HELP_CATEGORY:
        # Получаем id категории и загружаем для неё заметки
        category_id = int(info[1])
        notes = HelpNotes.objects.filter(category_id=category_id)
        keyboard = utils.get_help_notes_keyboard(notes)
        bot.edit_message_text(chat_id=chat_id, message_id=message_id,
                              text='Выберите интересующую вас заметку', reply_markup=keyboard)
    # Обработка выбора заметки
    elif mode == utils.HELP_NOTE:
        note_id = int(info[1])
        note = HelpNotes.objects.get(id=note_id)
        keyboard = utils.get_inline_keyboard([
            ('⬅️ Назад', [utils.ACTION_BACK_TO_NOTES, note.category_id])
        ])
        bot.edit_message_text(chat_id=chat_id, message_id=message_id,
                              text=(f'*{note.title}*\n'
                                    f'{note.text}\n\n'
                                    f'_Обновлено: {timezone.localtime(note.updated).strftime("%d.%m.%Y в %H:%M")}_'),
                              reply_markup=keyboard, parse_mode='Markdown')
    # Обработка удаления сообщения
    elif mode == utils.ACTION_CLOSE:
        # Удаление незаполненной задачей
        if teacher.status == CHOICE_DEPARTAMENT_MODE or teacher.status == INPUT_PROBLEM_MODE:
            Task.objects.filter(status=-1, director=teacher).delete()
            teacher.status = 0
            teacher.save()

            bot.edit_message_text(chat_id=chat_id, message_id=message_id,
                                  text='Постановка задачи отменена')
            bot.send_message(chat_id, 'Выберите нужный пункт', reply_markup=utils.get_main_menu_keyboard())
        else:
            bot.edit_message_text(chat_id=chat_id, message_id=message_id,
                                  text='Выберите нужный пункт')
    # Обработка возвращения к категориям
    elif mode == utils.ACTION_BACK_TO_CATEGORY:
        bot.edit_message_text(chat_id=chat_id, message_id=message_id,
                              text='Выберите нужную категорию', reply_markup=utils.get_help_category_keyboard())
    # Обработка возвращеня к заметкам в категории
    elif mode == utils.ACTION_BACK_TO_NOTES:
        category_id = int(info[1])
        notes = HelpNotes.objects.filter(category_id=category_id)
        bot.edit_message_text(chat_id=chat_id, message_id=message_id,
                              text='Выберите нужную заметку', reply_markup=utils.get_help_notes_keyboard(notes))
    # Обработка принятия поставленной задачи
    elif mode == utils.ACTION_APPLY_TASK:
        task_id = int(info[1])
        task = Task.objects.get(id=task_id)
        text = utils.clear_text(task.text)
        if task.status != 0:
            bot.edit_message_text(chat_id=chat_id, message_id=message_id,
                                  text=f'Данную заявку уже принял {task.executor.full_name}')
        else:
            task.executor = teacher
            task.status = 1
            task.save()
            bot.edit_message_text(chat_id=chat_id, message_id=message_id,
                                  text=f'Вы приняли заявку "{text}" на исполнение\n'
                                       f'Для управление задачами перейдите в "Список задач"')
            bot.send_message(task.director.chat_id,
                             f'Ваша заявка "{text}" принята на исполнение\n'
                             f'Исполнитель: [{task.executor}](tg://user?id={task.executor.chat_id})',
                             parse_mode='Markdown')
            # Создаем запись в истории
            TaskHistory.objects.create(
                created=timezone.localtime(timezone.now()),
                task=task,
                comment=f'{task.executor} принял задачу на исполнение'
            )
    # Обработка показа поставленных задач
    elif mode == utils.TASK_LIST_DELIVER:
        tasks = Task.objects.filter(director=teacher, status__in=[1, 2]).exclude(executor=teacher)
        keyboard = utils.get_list_task_keyboard(tasks, utils.TASK_SHOW_DELIVER)
        bot.edit_message_text('*Поставленные задачи*\n'
                              'Выберите нужную задачу для управления ей', chat_id,
                              message_id, reply_markup=keyboard, parse_mode='Markdown')
    # Обработка показа исполняемых задач
    elif mode == utils.TASK_LIST_EXECUTE:
        tasks = Task.objects.filter(executor=teacher, status=1).exclude(status__in=[-1, 3])
        keyboard = utils.get_list_task_keyboard(tasks, utils.TASK_SHOW_EXECUTE)
        bot.edit_message_text('*Выполняемые задачи*\n'
                              'Выберите нужную задачу для управление ей', chat_id,
                              message_id, reply_markup=keyboard, parse_mode='Markdown')
    # Показ задач, которые нужно подтвердить
    elif mode == utils.LIST_NEED_APPLY_TASKS:
        tasks = Task.objects.filter(director=teacher, status=2)
        keyboard = utils.get_list_task_keyboard(tasks, utils.TASK_NEED_APPLY)
        bot.edit_message_text('*Задачи, которые ожидают подтверждения от вас*\n'
                              'Выберите нужную задачу для управления ей', chat_id,
                              message_id, reply_markup=keyboard, parse_mode='Markdown')
    # Показ задач, которые ожидают подтверждения
    elif mode == utils.LIST_WAIT_APPLY_TASKS:
        tasks = Task.objects.filter(executor=teacher, status=2)
        keyboard = utils.get_list_task_keyboard(tasks, utils.TASK_WAIT_APPLY)
        bot.edit_message_text('*Задачи, которые ожидают подтверждения от постановщика*\n'
                              'Выберите нужную задачу для управления ей', chat_id,
                              message_id, reply_markup=keyboard, parse_mode='Markdown')
    # Показ меню, для выбора типа задач
    elif mode == utils.TASK_MAIN:
        keyboard = utils.get_type_task_keyboard(teacher)
        if keyboard.keyboard:
            bot.edit_message_text('Выберите нужную задачу для управление ей', chat_id,
                                  message_id, reply_markup=keyboard)
        else:
            bot.delete_message(chat_id, message_id)
            bot.send_message(chat_id, 'Не найдено активных задач')
    # Вывести информацию об задаче, которую выставил пользователь
    elif mode in (utils.TASK_SHOW_DELIVER, utils.TASK_NEED_APPLY):
        task_id = int(info[1])
        task = Task.objects.get(id=task_id)
        branch = task.branch if task.branch else 'Нет информации'
        deadline = timezone.localtime(task.deadline).strftime("%d.%m.%Y") if task.deadline else 'Не назначен'
        if task.executor:
            executor = f'[{task.executor}](tg://user?id={task.executor.chat_id})'
        else:
            executor = 'Не назначен'
        message = (f'Задача: {utils.clear_text(task.text)}\n'
                   f'Исполнитель: {executor}\n'
                   f'Филиал: {branch}\n'
                   f'Поставлена: {timezone.localtime(task.created).strftime("%d.%m.%Y в %H:%M")}\n'
                   f'Дедлайн: {deadline}')

        # Определяем ссылку для кнопки "Назад"
        if mode == utils.TASK_SHOW_DELIVER:
            back = utils.TASK_LIST_DELIVER
        elif mode == utils.TASK_NEED_APPLY:
            back = utils.LIST_NEED_APPLY_TASKS
            if task.comment:
                comment = utils.clear_text(task.comment)
            else:
                comment = 'Отсутствует'
            message = '\n'.join([message, f'Комментарий: {comment}'])
        else:
            return
        buttons = [
            [
                ('✅ Завершить', [utils.ACTION_CLOSE_DELIVER_TASK, task.id])
            ],
            ('⬅️ Назад', [back])
        ]
        # Кнопка отклонения, если задача на подтверждении
        if mode == utils.TASK_NEED_APPLY:
            buttons[0].append(('❌ Отклонить', [utils.ACTION_REJECT_TASK, task.id]))

        # Формируем клавиатуру для выбора действия
        keyboard = utils.get_inline_keyboard(buttons)
        bot.edit_message_text(message, chat_id, message_id,
                              reply_markup=keyboard, parse_mode='Markdown')
    # Обработка показа исполняемой задачи
    elif mode in (utils.TASK_SHOW_EXECUTE, utils.TASK_WAIT_APPLY):
        task_id = int(info[1])
        task = Task.objects.get(id=task_id)
        branch = task.branch if task.branch else 'Нет информации'
        deadline = timezone.localtime(task.deadline).strftime("%d.%m.%Y") if task.deadline else 'Не назначен'
        message = (f'Задача: {utils.clear_text(task.text)}\n'
                   f'Постановщик: [{task.director}](tg://user?id={task.director.chat_id})\n'
                   f'Филиал: {branch}\n'
                   f'Поставлена: {timezone.localtime(task.created).strftime("%d.%m.%Y в %H:%M")}\n'
                   f'Статус: {task.get_status_display()}\n'
                   f'Дедлайн: {deadline}')
        keyboard = types.InlineKeyboardMarkup()
        if task.status == 1:
            if mode == utils.TASK_SHOW_EXECUTE:
                keyboard.add(utils.get_inline_button('✅ Выполнена', [utils.ACTION_CLOSE_EXECUTE_TASK, task.id]))
            if not task.director == teacher:
                keyboard.add(utils.get_inline_button('📤 Передать задачу', [utils.ACTION_TRANSFER_TASK, task.id]))
            keyboard.add(utils.get_inline_button('📆 Перенести дедлайн', [utils.ACTION_TRANSFER_DEADLINE, task.id]))
        else:
            keyboard.add(utils.get_inline_button('✅ Повторить выполнение', [utils.ACTION_CLOSE_EXECUTE_TASK, task.id]))

        # Определяем клавишу назад
        if mode == utils.TASK_SHOW_EXECUTE:
            back = utils.get_inline_button('⬅️ Назад', [utils.TASK_LIST_EXECUTE])
        else:
            back = utils.get_inline_button('⬅️ Назад', [utils.LIST_WAIT_APPLY_TASKS])
        keyboard.add(back)
        bot.edit_message_text(message, chat_id, message_id,
                              reply_markup=keyboard, parse_mode='Markdown')
    # Обработка выбора отдела, куда направить задачу
    elif mode == utils.ACTION_CHOICE_DEPARTAMENT:
        # Обработка выбора нужного отдела
        departament_id = int(info[1])
        departament = Departament.objects.get(id=departament_id)
        task_type = 0 if teacher.office else 1
        if Task.objects.filter(director=teacher, status=-1):
            return
        # Создаем задачу, куда занесен только отдел и заказчик
        task = Task.objects.create(director=teacher, departament=departament, status=-1,
                                   created=timezone.localtime(timezone.now()), type=task_type)
        # Если пользователь - офисный сотрудник, то предлагаем ему выбрать дедлайн
        if teacher.office:
            # Получаем пользователей выбранного отдела, за исключением текущего пользователя
            users = Teacher.objects.filter(is_active=True, roles__departament=departament).exclude(
                id=teacher.id).exclude(chat_id=0)
            keyboard = utils.get_inline_keyboard()
            for user in users:
                print(user)
                keyboard.add(utils.get_inline_button(user.get_short_name,
                                                     [utils.ACTION_CHOICE_EXECUTE, task.id, user.id]))
            keyboard.add(utils.get_inline_button('Отделу', [utils.SEND_ALL_DEPARTMENT, task.id]))
            keyboard.add(utils.get_inline_button('❌ Отмена', [utils.ACTION_CLOSE]))
            bot.edit_message_text('Выберите нужного исполнителя', chat_id, message_id,
                                  reply_markup=keyboard)
        else:
            teacher.status = INPUT_PROBLEM_MODE
            teacher.save()

            bot.delete_message(chat_id, message_id)
            bot.send_message(chat_id, 'Напишите задачу и отправьте её боту',
                             reply_markup=utils.get_keyboard(['❌ Отмена']))
    # Закрытие задачи, которую мы выполняем
    elif mode == utils.ACTION_CLOSE_EXECUTE_TASK:
        task_id = int(info[1])
        # Статус задачи устанавливаем в Ожидает подтверждения
        task = Task.objects.get(id=task_id)
        # Если задачу, которую мы закрываем уже была закрыта ранее, сообщаем об этом
        if task.status == 3:
            bot.edit_message_text('Данная задача была завершена ранее', chat_id, message_id)
            return
        elif task.executor != teacher:
            bot.edit_message_text('Вы не являетесь исполнителем данной задачи', chat_id, message_id)
            return
        text = utils.clear_text(task.text)
        # Если это самозадача
        if task.director == teacher:
            task.status = 3
            message = f'Задача "{text}" выполнена'
            task.closed = now
        else:
            # Обрабатываем только первое нажатие кнопки
            if teacher.status == INPUT_COMMENT_FOR_CLOSE_TASK:
                return
            teacher.status = INPUT_COMMENT_FOR_CLOSE_TASK
            teacher.save()
            # Заносим в историю передачу задачи
            TaskHistory.objects.create(
                task=task,
                comment=f'{task.executor} завершил выполнения задачи, задача отправлена на подтверждение',
                created=now,
                need_action=True,
            )
            bot.delete_message(chat_id, message_id)
            bot.send_message(chat_id, '📝 Введите комментарий к задаче 📝',
                             reply_markup=utils.get_keyboard(['❎ Без комментария','❌ Отмена']))
            return
        task.save()
        bot.edit_message_text(message, chat_id, message_id, parse_mode='Markdown')
    # Закрытие задачи, которую мы выставили
    elif mode == utils.ACTION_CLOSE_DELIVER_TASK:
        task_id = int(info[1])
        task = Task.objects.get(id=task_id)
        if task.status == 3:
            bot.edit_message_text('Данная задача была завершена ранее', chat_id, message_id)
            return
        text = utils.clear_text(task.text)
        # Завершаем задачу
        task.status = 3
        task.closed = timezone.localtime(timezone.now())
        task.save()
        # Заносим в историю завершения задачи
        TaskHistory.objects.create(
            task=task,
            comment=f'{task.director} подтвердил выполнение задачи',
            created=now,
        )
        # Отправляем уведомление, что задача решена, если есть исполнитель
        if task.executor:
            message = f'{task.director} отметил задачу "{text}" выполненной'
            bot.send_message(task.executor.chat_id, message)
        bot.edit_message_text(f'Задача "{text}" закрыта', chat_id, message_id)

    # Обработка отправки задачи на доработку
    elif mode == utils.ACTION_REJECT_TASK:
        task_id = int(info[1])
        task = Task.objects.get(id=task_id)
        # Меняем статус на Выполняется
        task.status = 1
        task.save()

        # Заносим в историю передачу задачи
        TaskHistory.objects.create(
            task=task,
            comment=f'{task.director} отправил задачу на доработку',
            created=now,
        )

        # Отправляем уведомление исполнителю на доработку
        bot.send_message(task.executor.chat_id,
                         f'{task.director} отказал в закрытии здачи "{utils.clear_text(task.text)}"')
        bot.edit_message_text('Задача отправлена на доработку', chat_id, message_id)
    # Обработка передачи задачи
    elif mode == utils.ACTION_TRANSFER_TASK:
        task_id = int(info[1])
        task = Task.objects.get(id=task_id)
        # Если задачу, которую мы передаем уже не наша или завершена
        if task.executor != teacher:
            bot.edit_message_text('Вы не являетесь исполнителем данной задачи', chat_id, message_id)
        elif task.status == 3:
            bot.edit_message_text('Данная задача была завершена ранее', chat_id, message_id)
        department_users = Teacher.objects.filter(
            roles__departament=task.departament,
            is_active=True).exclude(id__in=[0, task.director.id, task.executor.id])
        keyboard = types.InlineKeyboardMarkup()
        for user in department_users:
            keyboard.add(utils.get_inline_button(
                user.full_name, [utils.ACTION_CHOICE_USER_FOR_TRANSFER_TASK, task.id, user.id]
            ))
        keyboard.add(utils.get_inline_button('⬅️ Назад', [utils.TASK_SHOW_EXECUTE, task.id]))
        bot.edit_message_text('Выберите сотрудника, которому вы хотите передать задачу',
                              chat_id, message_id, reply_markup=keyboard)
    # Обработка передачи задачи пользователю
    elif mode == utils.ACTION_CHOICE_USER_FOR_TRANSFER_TASK:
        task_id = int(info[1])
        user_id = int(info[2])
        task = Task.objects.get(id=task_id)
        new_executor = Teacher.objects.get(id=user_id)

        # Если задачу, которую мы передаем уже не наша или завершена
        if task.executor != teacher:
            bot.edit_message_text('Вы не являетесь исполнителем данной задачи', chat_id, message_id)
        elif task.status == 3:
            bot.edit_message_text('Данная задача была завершена ранее', chat_id, message_id)
        text = utils.clear_text(task.text)
        # Редактируем сообщение
        keyboard = utils.get_inline_keyboard([
            ('⬅️ Назад', [utils.TASK_LIST_EXECUTE]),
        ])

        # Заносим в историю передачу задачи
        TaskHistory.objects.create(
            task=task,
            comment=f'{task.executor} передал задачу {new_executor}',
            created=now,
        )

        bot.edit_message_text(f'Вы передали задачу {utils.get_user_link(new_executor)}',
                              chat_id, message_id, reply_markup=keyboard, parse_mode='Markdown')
        # Информируем постановщика, что исполнитель изменился
        bot.send_message(task.director.chat_id,
                         f'У вашей задачи "{text}" новый исполнитель: {utils.get_user_link(new_executor)}',
                         parse_mode='Markdown')
        # Информируем нового исполнителя, что ему передали задачу
        bot.send_message(new_executor.chat_id,
                         f'{utils.get_user_link(task.executor)} передал вам задачу "{text}"',
                         parse_mode='Markdown')
        task.executor = new_executor
        task.save()
    # Обработка выбора дедлайна
    elif mode == utils.ACTION_CHOICE_DEADLINE:
        # Получаем кол-во дней до дедлайна
        days = int(info[1])
        task_id = info[2]
        # Если не передан id задачи
        if task_id == -1:
            # Получаем задачу, которая заполняется
            if days >= 0:
                task = Task.objects.get(director=teacher, status=-1)
                task.deadline = now + timedelta(days=days)
                task.save()

            teacher.status = INPUT_PROBLEM_MODE
            teacher.save()

            bot.delete_message(chat_id, message_id)
            bot.send_message(chat_id, 'Напишите задачу и отправьте её боту',
                             reply_markup=utils.get_keyboard(['❌ Отмена']))
        else:
            task = Task.objects.get(id=task_id)
            # Если это самозадача, то просто переносим дедлайн
            if task.executor == task.director:
                if days >= 0:
                    task.deadline = now + timedelta(days=days)
                else:
                    task.deadline = None

                task.save()
                bot.edit_message_text('Дедлайн изменен', chat_id, message_id)
            else:
                # Создаём запрос в истории
                TaskHistory.objects.create(
                    task=task,
                    comment='',
                    need_action=True,
                    created=now,
                    info=json.dumps({'days': days})
                )
                # Сообщаем пользователю, что он должен ввести причину
                teacher.status = INPUT_REASON_MODE
                teacher.save()
                bot.delete_message(chat_id, message_id)
                bot.send_message(chat_id, 'Введите причину переноса дедлайна',
                                 reply_markup=utils.get_keyboard(['❌ Отмена']))
    # Ввод кастомного дедлайна
    elif mode == utils.CUSTOM_DEADLINE:
        task_id = info[1]
        # Предлагаем ввести дедлайн
        # Если задча, не передана, значит это создание
        if task_id == -1:
            bot.delete_message(chat_id, message_id)
        # Иначе создаем запись в истории
        else:
            TaskHistory.objects.create(
                task=Task.objects.get(id=task_id),
                comment='',
                need_action=True,
                created=now,
            )
        bot.send_message(chat_id, 'Введите дату дедлайна в формате дд.мм.гггг\n'
                                  'Например: 8.11.2019',
                         reply_markup=utils.get_keyboard(['❌ Отмена']))
        teacher.status = INPUT_DEADLINE_MODE
        teacher.save()
    # Обработка выбора исполнителя
    elif mode == utils.ACTION_CHOICE_EXECUTE:
        # Получаем id задачи
        task_id = info[1]
        user_id = info[2]
        print(task_id)
        # Если id = -1, значит создаем задачу
        # где пользователь заказчик и исполнитель
        if task_id == -1:
            Task.objects.create(
                director=teacher,
                executor=teacher,
                status=-1,
                created=now,
                type=2,
            )
        # Иначе задаче присваиваем конкретного исполнителя
        else:
            user = Teacher.objects.get(id=user_id)
            task = Task.objects.get(id=task_id)
            task.executor = user
            task.save()

        # Предлагаем выбор дедлайна
        bot.edit_message_text('Выберите дедлайн для задачи', chat_id, message_id,
                              reply_markup=utils.get_deadlines_keyboard())
    # Обработка отправки всему отделу
    elif mode == utils.SEND_ALL_DEPARTMENT:
        bot.edit_message_text('Выберите дедлайн для задачи', chat_id, message_id,
                              reply_markup=utils.get_deadlines_keyboard())
    # Обработка переноса дедлайна
    elif mode == utils.ACTION_TRANSFER_DEADLINE:
        task_id = info[1]
        bot.edit_message_text('Выберите дедлайн для задачи', chat_id, message_id,
                              reply_markup=utils.get_deadlines_keyboard(task_id))
    # Подтверждение переноса дедлайна
    elif mode == utils.APPLY_TRANSFER_DEADLINE:
        # Получаем задачу для которой переносися дедлайн и кол-во дней
        task = Task.objects.get(id=info[1])
        days = info[2]
        if days == -1:
            task.deadline = None
        else:
            task.deadline = now + timedelta(days=days)
        task.save()

        bot.delete_message(chat_id, message_id)
        bot.send_message(chat_id, 'Дедлайн изменен')

        bot.send_message(task.executor.chat_id,
                         f'{task.director} подтвердил перенос дедлайна по задаче "{task.text}"')

        # Создаем запись в истории задач
        TaskHistory.objects.create(
            task=task,
            comment=f'{task.director} подтвердил перенос дедлайна',
            created=now,
        )

    elif mode == utils.CANCEL_TRANSFER_DEADLINE:
        task = Task.objects.get(id=info[1])

        bot.delete_message(chat_id, message_id)
        bot.send_message(chat_id, 'Вы отклонили перенос дедлайна')

        bot.send_message(task.executor.chat_id, f'{task.director} отменил перенос дедлайна по задача "{task.text}"')

        # Создаем запись в истории задач
        TaskHistory.objects.create(
            task=task,
            comment=f'{task.director} отклонил перенос дедлайна',
            created=now,
        )
    # Создание заказа на детали
    elif mode == utils.CREATE_DETAIL_ORDER:
        # Проверяем, что пользователь ничего другого не делает
        if teacher.status != 0:
            bot.send_message(chat_id, 'У вас есть незавершенные действия')
            return

        bot.delete_message(chat_id, message_id)

        # Создаем заказ с незаполненным филиалом
        order = Order(
            created=timezone.localtime(timezone.now()),
            teacher=teacher
        )
        order.save()

        # Получаем последние филиалы
        logs = utils.get_branches_on_weak(timezone.localtime(timezone.now()) + timedelta(days=1), teacher)
        keyboard = utils.get_keyboard(list(set([log.branch.name for log in logs])) + [['🏢 Филиалы', '❌ Отмена']])

        bot.send_message(chat_id, 'Введите название филиала, для которого вы хотите заказать детали',
                         reply_markup=keyboard)
        teacher.status = INPUT_BRANCH_FOR_ORDER
        teacher.save()
    # Обработка выбора занятия
    else:
        if teacher.status == 2:
            data_id = int(call.data)
            lesson = Lesson.objects.get(id=data_id)
            bot.edit_message_text(chat_id=chat_id,
                                  message_id=call.message.message_id,
                                  text='Вы выбрали: {}'.format(lesson))

            temp = TempData.objects.get(teacher=teacher)
            branch = temp.branch
            date_log = temp.date
            time = temp.time

            path = save_temp_files(teacher, branch, temp)

            # создаем лог и удаляем временную запись
            log = LogSending(lesson=lesson, teacher=teacher, branch=branch,
                             date=date_log, time=time, photos=path)
            log.save()
            temp.delete()
            # Формируем сообщение для ответа
            if log.time_before < 0:
                msg = text_after_lesson.format(log.date, log.time.strftime("%H:%M"),
                                               log.lesson, log.time_before)
            else:
                msg = text_before_lesson.format(log.date, log.time.strftime("%H:%M"),
                                                log.lesson, log.time_before)

            bot.send_message(chat_id, msg, reply_markup=utils.get_main_menu_keyboard())

            # отправляем сообщение администраторам
            send_notification(log)

            teacher.status = 0
            teacher.save()


# ======================== НАЧАЛО РАБОТЫ ====================== #
@bot.message_handler(commands=['start'])
def proc_start(message):
    chat_id = message.chat.id
    try:
        teacher = Teacher.objects.get(chat_id=chat_id)
        bot.send_message(chat_id,
                         'Добро пожаловать, {}'.format(teacher.full_name),
                         reply_markup=utils.get_main_menu_keyboard())
    except Teacher.DoesNotExist:
        try:
            NewUser.objects.get(chat_id=chat_id)
            bot.send_message(chat_id, 'С возвращением')
        except NewUser.DoesNotExist:
            NewUser.objects.create(chat_id=chat_id, status=0)

        keyboard = utils.get_keyboard(['🔑 Авторизация'])
        bot.send_message(chat_id, 'Добро пожаловать, для начала авторизируйтесь /auth',
                         reply_markup=keyboard)


# ========================= ВЫХОД ======================== #
@bot.message_handler(commands=['exit'])
@utils.teacher_required(bot=bot)
def teacher_exit(message):
    chat_id = message.chat.id
    teacher = Teacher.objects.get(chat_id=chat_id)
    teacher.chat_id = 0
    teacher.status = 0
    teacher.save()
    NewUser.objects.create(chat_id=chat_id, status=0)
    keyboard = utils.get_keyboard(['🔑 Авторизация'])
    bot.send_message(chat_id, 'Вы вышли из системы:(', reply_markup=keyboard)


# ===================== СПИСОК ФИЛИАЛОВ ======================== #
@bot.message_handler(func=lambda msg: msg.content_type == 'text' and (
        msg.text.strip().lower() == '🏢 филиалы'
        or msg.text.strip().lower() == '/list'
))
@utils.teacher_required(bot=bot)
def branch_list(message):
    chat_id = message.chat.id
    message = '<b>Список филиалов</b>\n'
    for branch in Branch.objects.filter(is_active=True).order_by('num_code'):
        message += '{} - {}\n'.format(branch.num_code, branch.name)
    bot.send_message(chat_id, message, parse_mode='HTML')


# ===================== СООБЩЕНИЕ О ПОМОЩИ ====================== #
@bot.message_handler(commands=['help'])
@bot.message_handler(func=lambda msg: msg.content_type == 'text' and (
        msg.text.strip().lower().find('справка') != -1
))
@utils.teacher_required(bot=bot)
def print_help(message):
    chat_id = message.chat.id
    # Формируем клавиатуру категорий
    keyboard = utils.get_help_category_keyboard()
    bot.send_message(chat_id, 'Выберите нужную категорию', parse_mode='HTML', reply_markup=keyboard)


# =================== ОТМЕНА ДЕЙСТВИЙ ===================== #
@bot.message_handler(commands=['cancel'])
@utils.teacher_required(bot=bot)
def proc_cancel(message):
    chat_id = message.chat.id

    teacher = Teacher.objects.get(chat_id=chat_id)

    teacher.status = 0
    teacher.save()

    # очищаем все временные данные
    for tempData in TempData.objects.filter(teacher=teacher):
        tempData.delete()
        bot.send_message(chat_id, 'Удалена временная запись: {}'.format(tempData))

    Task.objects.filter(director=teacher, status=-1).delete()

    path = os.path.join(TEMP_DIR, teacher.slug_name)  # формируем путь до временной директории
    shutil.rmtree(path, ignore_errors=True)  # удаляем временную директорию
    bot.send_message(chat_id, 'Действия отменены', reply_markup=utils.get_main_menu_keyboard())


# ====================== АВТОРИЗАЦИЯ ========================== #
@bot.message_handler(commands=['auth'])
@bot.message_handler(
    func=lambda msg: msg.content_type == 'text' and msg.text.strip().lower() == '🔑 авторизация')
def proc_auth(message):
    chat_id = message.chat.id
    try:
        Teacher.objects.get(chat_id=chat_id)
        bot.send_message(chat_id, 'Вы уже авторизированы')
    except Teacher.DoesNotExist:
        # Проверяем существание пользователя
        try:
            new_user = NewUser.objects.get(chat_id=chat_id)
            new_user.status = 1
            new_user.save()
        except NewUser.DoesNotExist:
            NewUser.objects.create(chat_id=chat_id, status=1)

        bot.send_message(
            chat_id, """
<b>Авторизация</b>
Для авторизации введите Ваши ФИО в формате: Фамилия Имя Отчество
""",
            parse_mode='HTML',
            reply_markup=utils.get_keyboard([])
        )


# =================== ОТПРАВИТЬ ОТЧЁТ ========================#
@bot.message_handler(
    func=lambda msg: msg.content_type == 'text' and msg.text.strip().lower() == '✉ отправить отчёт'
)
@utils.teacher_required(bot=bot)
def proc_message_send_log(message):
    chat_id = message.chat.id
    bot.send_message(chat_id, 'Для отправки отчёта загрузите фото')


# ====================== ИНФО ============================ #
@bot.message_handler(commands=['info'])
@utils.teacher_required(bot=bot)
def proc_info(message):
    chat_id = message.chat.id
    teacher = Teacher.objects.get(chat_id=chat_id)
    server_datetime = timezone.localtime(timezone.now()).strftime('%d.%m.%Y %H:%M')
    status = teacher.status
    bot.send_message(chat_id, 'Информация\nДата: {}\nСтатус: {}'.format(server_datetime, status))


# ===================== УВЕДОМЛЕНИЯ ====================== #
@bot.message_handler(commands=['on', 'off'])
@utils.teacher_required(bot=bot)
def notification(message):
    text = message.text
    chat_id = message.chat.id

    try:
        teacher = Teacher.objects.get(chat_id=chat_id)  # получаем пользователя по chat_id
        if teacher.additional & 1 == 1:  # если он администратор, то меняем режим уведомлений
            if text == '/on':
                teacher.additional = teacher.additional | 2  # или
                bot.send_message(chat_id, 'Уведомления включены\n/off - выключить уведомления')
            elif text == '/off':
                teacher.additional = teacher.additional ^ 2  # исключающее или
                bot.send_message(chat_id, 'Уведомления отключены\n/on - включить уведомления')
            teacher.save()
        else:
            bot.send_message(chat_id, 'Данная команда вам недоступна')
    except Exception as e:
        bot.send_message(chat_id, 'Ошибка: {}'.format(e))


# ===================== ОБРАБОТКА ПОДГОТОВКИ ========================= #
@bot.message_handler(
    func=lambda msg: msg.content_type == 'text' and msg.text.strip().lower() == '📝 подготовка'
)
def proc_prepare(message):
    chat_id = message.chat.id
    try:
        teacher = Teacher.objects.get(chat_id=chat_id)
    except Teacher.DoesNotExist:
        bot.send_message(chat_id, message_for_user)
        return

    if teacher.status != 1:
        bot.send_message(chat_id, 'Вы не загрузили фото')
        return
    # получаем информацию и удаляем запись из таблицы
    temp_log = TempData.objects.get(teacher=teacher)
    date_log = temp_log.date
    time = temp_log.time

    weak = utils.get_weak(date_log)  # получаем [начало недели, конец недели]

    # относительный путь до постоянной дирректории
    path = os.path.join(settings.MEDIA_DIR, 'prepare',
                        teacher.slug_name, date_log.strftime('%d-%m-%Y'))

    # абсолютный путь до временной директории
    from_path = os.path.join(TEMP_DIR, teacher.slug_name)

    # абсолютный путь до постоянной директории
    to_path = os.path.join(settings.BASE_DIR, path)

    #  Получаем ифнормацию об активности приёма подготовки
    on_prepare = Settings.get_value('on_prepare')
    if not on_prepare:
        # Отключение принятия подготовки
        shutil.rmtree(from_path)
        bot.send_message(chat_id, 'Данная функция недоступна',
                         reply_markup=utils.get_main_menu_keyboard())
        temp_log.delete()
        return

    # перемещаем файлы
    move_files(from_path, to_path, delete=True)

    # проверяем был ли на этой недели фото подготовки
    try:
        # дополняем подготовку на этой недели
        Preparation.objects.get(teacher=teacher, date__gte=weak['start'], date__lte=weak['end'])
    except Preparation.DoesNotExist:
        # создаем запись о подготовке
        Preparation.objects.create(date=date_log, time=time, teacher_id=teacher.id, photos=path)

    bot.send_message(chat_id, 'Фото с подготовки принято;)',
                     reply_markup=utils.get_main_menu_keyboard())
    temp_log.delete()


# ====================== ОБРАБОТКА ПРОЧЕЕ ============================= #
@utils.teacher_required(bot=bot)
@bot.message_handler(
    func=lambda msg: msg.content_type == 'text' and msg.text.strip().lower() == '📜 прочее'
)
def proc_prepare(message):
    chat_id = message.chat.id
    teacher = Teacher.objects.get(chat_id=chat_id)
    keyboard = types.InlineKeyboardMarkup()
    path = f'{settings.ALLOWED_HOSTS[0]}/profile/{teacher.password}/logs/'
    keyboard.add(types.InlineKeyboardButton(text='📁 История отчётов', url=path))
    keyboard.add(utils.get_inline_button('🎁 Заказать детали', [utils.CREATE_DETAIL_ORDER]))
    bot.send_message(chat_id, 'Дополнительное меню', reply_markup=keyboard)


# ===================== ОБРАБОТКА СОРТИРОВКИ ========================= #
@bot.message_handler(
    func=lambda msg: msg.content_type == 'text' and msg.text.strip().lower() == '📦 сортировка'
)
@utils.teacher_required(bot=bot)
def proc_sort(message):
    """
    Обработка команды для сортировки
    :param message:
    :return:
    """
    chat_id = message.chat.id
    teacher = Teacher.objects.get(chat_id=chat_id)
    logs = utils.get_branches_on_weak(timezone.localtime(timezone.now()) + timedelta(days=1), teacher)
    keyboard = utils.get_keyboard(list(set([log.branch.name for log in logs])) + [['🏢 Филиалы', '⬅️ Назад']])
    if teacher.status == 1:
        bot.send_message(chat_id,
                         'Выберите нужный филиал или укажите его самостоятельно, введя его название или код',
                         reply_markup=keyboard)
    elif teacher.status == 0:
        # Создаем временную запись
        TempData.objects.create(
            date=timezone.localtime(timezone.now()),
            time=datetime.time(timezone.localtime(timezone.now())),
            teacher_id=teacher.id,
        )
        msg = Settings.get_value('message_for_sorting')
        bot.send_message(chat_id, msg, parse_mode='HTML', reply_markup=keyboard)
    else:
        bot.send_message(chat_id, 'У вас есть незавершенные действия')
        return
    teacher.status = 3  # Меняем статус преподавателя
    teacher.save()


# ===================== ОБРАБОТКА КНОПКИ НАЗАД ========================= #
@bot.message_handler(
    func=lambda msg: msg.content_type == 'text' and msg.text.strip().lower() == '⬅️ назад'
)
@utils.teacher_required(bot=bot)
def proc_back(message):
    """
    Обработка команды для сортировки
    :param message:
    :return:
    """
    chat_id = message.chat.id
    teacher = Teacher.objects.get(chat_id=chat_id)

    teacher.status = 1
    teacher.save()
    bot.send_message(chat_id, 'Выберите нужное действие',
                     reply_markup=utils.get_action_keyboard(utils.ACTION_MODE_PHOTO))


# ===================== ТРЕВОЖНАЯ КНОПКА ========================= #
@bot.message_handler(
    func=lambda msg: msg.content_type == 'text' and msg.text == '🆘'
)
@utils.teacher_required(bot=bot)
def proc_back(message):
    """
    Обработка команды для сортировки
    :param message:
    :return:
    """
    chat_id = message.chat.id
    teacher = Teacher.objects.get(chat_id=chat_id)

    teacher.status = INPUT_DANGER_REASON
    teacher.save()
    bot.send_message(chat_id, 'Введите сообщение',
                     reply_markup=utils.get_keyboard(['❌ Отмена']))


# =========================== РЕКОМЕНДУЕМЫЕ ФИЛИАЛЫ ============================= #
@bot.message_handler(
    func=lambda msg: msg.content_type == 'text' and msg.text.strip().lower() == '🖼 фото кабинета')
@utils.teacher_required(bot=bot)
def proc_actual_branch(message):
    chat_id = message.chat.id
    teacher = Teacher.objects.get(chat_id=chat_id)
    # Получаем три последних филиалов и формируем клавиатуру
    branches = utils.get_last_actual_branches(timezone.localtime(timezone.now()), teacher)[:3]
    keyboard = utils.get_keyboard([branch[0].name.upper() for branch in branches] + [['🏢 Филиалы', '⬅️ Назад']])

    msg = 'Выберите нужный филиал или укажите его самостоятельно, введя его название или код'
    if not branches:
        msg = 'Укажите филиал, введя его название или код'
    bot.send_message(chat_id, msg, reply_markup=keyboard)


# ================= ПОСЛЕДНИЕ ФИЛИАЛЫ НА КОТОРЫХ БЫЛИ ЗАНЯТИЯ ============================= #
@bot.message_handler(
    func=lambda msg: msg.content_type == 'text' and msg.text.strip().lower() == '📷 фото с занятий')
@utils.teacher_required(bot=bot)
def proc_actual_branch(message):
    if not settings.DRIVE_FOLDER:
        return
    chat_id = message.chat.id
    teacher = Teacher.objects.get(chat_id=chat_id)
    # Ставим статус в 10 (Ожидание отправки филиала и даты)
    if not teacher.status == 1:
        bot.send_message(chat_id, 'Вы не загрузили фото',
                         reply_markup=utils.get_main_menu_keyboard())
        return
    teacher.status = 10
    teacher.save()
    logs = utils.get_branches_on_weak(timezone.localtime(timezone.now()) + timedelta(days=1), teacher)
    keyboard = utils.get_keyboard([f'{log.branch.name}: {log.date.strftime("%d.%m.%Y")}' for log in logs] +
                                  ['⬅️ Назад'])

    msg = 'Выберите нужные филиал и дату'
    if not logs:
        proc_cancel(message)
        msg = 'Не найдено занятий на этой недели, вы не можете отправить фото с занятий'
        keyboard = utils.get_main_menu_keyboard()
    bot.send_message(chat_id, msg, reply_markup=keyboard)


# =========================== ОТМЕНА ЗАГРУЗКИ ============================= #
@bot.message_handler(
    func=lambda msg: msg.content_type == 'text' and msg.text.strip().lower() in ['❌ отмена загрузки', '❌ отмена'])
def proc_cancel_text(message):
    chat_id = message.chat.id

    teacher = Teacher.objects.get(chat_id=chat_id)

    teacher.status = 0
    teacher.save()

    # очищаем все временные данные
    for tempData in TempData.objects.filter(teacher=teacher):
        tempData.delete()
    # Получаем задачи, которые заполняются
    for task in Task.objects.filter(status=-1):
        task.delete()

    for task in TaskHistory.objects.filter(task__executor=teacher, need_action=True):
        task.delete()

    for order in Order.objects.filter(teacher=teacher, branch=None):
        order.delete()

    path = os.path.join(TEMP_DIR, teacher.slug_name)  # формируем путь до временной директории
    shutil.rmtree(path, ignore_errors=True)  # удаляем временную директорию
    bot.send_message(chat_id, 'Отмена действий', reply_markup=utils.get_main_menu_keyboard())


# ========================== ЗАПРОС НА ПОМОЩЬ ============================== #
@bot.message_handler(
    func=lambda msg: msg.content_type == 'text' and msg.text.strip().lower() == '🔔 новая задача'
)
def proc_request_help(message: types.Message):
    chat_id = message.chat.id
    teacher = Teacher.objects.get(chat_id=chat_id)
    # Если есть не заполненная задача сообщаем об этом
    if teacher.status in (CHOICE_DEPARTAMENT_MODE, INPUT_PROBLEM_MODE, INPUT_DEADLINE_MODE, INPUT_REASON_MODE):
        bot.send_message(chat_id, 'Вы не завершили процесс создания/изменения задачи, '
                                  'для того, чтобы отменить его, нажмите "❌ Отменить задачу"',
                         reply_markup=utils.get_inline_keyboard(
                             [('❌ Отменить задачу', [utils.ACTION_CLOSE])]))
        return
    teacher.status = CHOICE_DEPARTAMENT_MODE
    teacher.save()
    bot.send_message(chat_id, 'Выберите отдел, которому вы хотите поставить задачу',
                     reply_markup=utils.get_departament_keyboard(teacher))


# ===================== ОБРАБОТКА ЗАПРОСА ЗАДАЧ ========================= #
@bot.message_handler(
    func=lambda msg: msg.content_type == 'text' and msg.text.strip().lower() == '📝 список задач'
)
def proc_task(message):
    chat_id = message.chat.id
    teacher = Teacher.objects.get(chat_id=chat_id)

    keyboard = utils.get_type_task_keyboard(teacher)
    if len(keyboard.keyboard) > 2:
        msg = '*Задачи*\n\nВыберите нужные вам задачи'
    else:
        msg = '*Задачи*\n\nУ вас нет текущих задач.\nЧтобы добавить задачу выберите "🔔 Новая задача"'
    bot.send_message(chat_id, msg, reply_markup=keyboard, parse_mode='Markdown')


# ================= ПОКАЗ ГЛАВНОЙ КЛАВИАТУРЫ ======================== #
@bot.message_handler(commands=['show'])
def proc_show_main_keyboard(message):
    chat_id = message.chat.id
    teacher = Teacher.objects.get(chat_id=chat_id)
    if teacher.status == 0:
        """Показ главной клавиатуры"""
        bot.send_message(chat_id, 'Выберите нужный пункт', reply_markup=utils.get_main_menu_keyboard())


# ===================== ОБРАБОТКА ТЕКСТА ======================== #
@bot.message_handler(content_types=['text'])
def proc_all_messages(message):
    chat_id = message.chat.id  # извлекаем id чата
    try:
        text = message.text.strip()  # извлекаем текст сообщения
    except Exception as e:
        text = message.caption.strip()
        if text.lower() == 'подготовка':
            proc_prepare(message)
            return
    # проверяем авторизацию пользователя
    new_user = None
    teacher = None
    # Текущее время
    now = timezone.localtime(timezone.now())
    try:
        teacher = Teacher.objects.get(chat_id=chat_id)
    except Teacher.DoesNotExist:
        try:
            new_user = NewUser.objects.get(chat_id=chat_id)
        except NewUser.DoesNotExist:
            new_user = NewUser.objects.create(chat_id=chat_id, status=0)

    # если пользователь авторизован
    if teacher:
        # Пробуем определить филиал
        logs_for_teacher = LogSending.objects.filter(teacher=teacher, date=now).order_by('-time')
        branch = None
        if logs_for_teacher.count():
            branch = logs_for_teacher[0].branch

        if teacher.status == INPUT_DANGER_REASON:
            text = utils.clear_text(text)
            teacher.status = 0
            teacher.save()
            # Рассылка причины по нужным отделам
            receivers = Teacher.objects.filter(roles__danger_notification=True)
            mailing_bot('❗Уведомление❗', text, receivers)
            bot.send_message(chat_id, 'Предупреждение отправлено',
                             reply_markup=utils.get_main_menu_keyboard())

        elif teacher.status == INPUT_COMMENT_FOR_CLOSE_TASK:
            text = utils.clear_text(text)
            task_history = TaskHistory.objects.filter(task__executor=teacher, need_action=True)[0]

            task_history.need_action = False
            task_history.save()
            task = task_history.task
            task.comment = text
            task.status = 2
            task.save()
            teacher.status = 0
            teacher.save()
            if text == '❎ Без комментария':
                text = 'Отсутствует'
            # Отправляем сообщение с клавиатурой постановщику задачи
            keyboard = utils.get_inline_keyboard()
            keyboard.row(
                utils.get_inline_button('✅ Принять', [utils.ACTION_CLOSE_DELIVER_TASK, task.id]),
                utils.get_inline_button('❌ Отклонить', [utils.ACTION_REJECT_TASK, task.id])
            )
            bot.send_message(task.director.chat_id, f'{utils.get_user_link(task.executor)} '
                                                    f'отметил задачу "{utils.clear_text(task.text)}" выполненной\n'
                                                    f'Комментарий: {text}',
                             reply_markup=keyboard, parse_mode='Markdown')
            bot.send_message(chat_id, f'Задача "{utils.clear_text(task.text)}" отправлена на подтверждение',
                             reply_markup=utils.get_main_menu_keyboard())
        # Если пользователь отправил проблему
        # то создаём задачу и отправляем уведомление
        elif teacher.status == INPUT_PROBLEM_MODE:
            # Получаем не заполненную задачу
            task = Task.objects.get(status=-1, director=teacher)
            task.status = 0
            task.branch = branch
            task.text = message.text
            text = utils.clear_text(task.text)

            # Если исполнителем является пользователь, сообщаем, что задача поставлена
            if task.executor == teacher:
                bot.send_message(chat_id, f'Вы поставили себе задачу "{text}"\n'
                                          f'управлять задачами можно через "Список задач"',
                                 reply_markup=utils.get_main_menu_keyboard())
                task.status = 1
            elif task.executor:
                task.status = 1
                bot.send_message(chat_id, f'Задача успешно создана\n'
                                          f'Исполнитель {task.executor}',
                                 reply_markup=utils.get_main_menu_keyboard())
                # Добавляем запись о создании задачи в историю
                TaskHistory.objects.create(
                    task=task,
                    comment=f'{task.director} создал задачу и назначил исполнителя: {task.executor}',
                    created=now,
                )
                # Отправляем уведомление исполнителю
                bot.send_message(
                    task.executor.chat_id,
                    f'*Уведомление*\n'
                    f'{utils.get_user_link(task.director)} поставил вам задачу "{text}"\n'
                    f'Дедлайн: {task.deadline.strftime("%d.%m.%Y") if task.deadline else "Не назначен"}',
                    parse_mode='Markdown')
            else:
                send_task_notification(task)
                bot.send_message(chat_id,
                                 f'Ваша задача отправлена в {task.departament}, ожидайте, когда её примут',
                                 reply_markup=utils.get_main_menu_keyboard())
                # Создаём запись в истории
                TaskHistory.objects.create(
                    task=task,
                    comment=f'{task.director} создал задачу для отдела {task.departament}',
                    created=now,
                )
            task.save()
            teacher.status = 0
            teacher.save()
        # Режим ввода причины переноса дедлайна
        elif teacher.status == INPUT_REASON_MODE:
            # Получаем задачу, для которой вводилась причина
            task_history = TaskHistory.objects.get(task__executor=teacher, need_action=True)
            task = task_history.task
            days = json.loads(task_history.info)['days']
            if days == -1:
                msg = f'{teacher} запросил перенос дедлайна на "Без дедлайна"\n Причина: {message.text}'
            else:
                msg = f'{teacher} запросил перенос дедлайна на {days} дней/дня\n Причина: {message.text}'
            task_history.comment = msg

            # Клавиатура для подтверждения переноса
            keyboard = utils.get_inline_keyboard([
                ('✅ Подтвердить', (utils.APPLY_TRANSFER_DEADLINE, task.id, days)),
                ('❌ Отклонить', (utils.CANCEL_TRANSFER_DEADLINE, task.id)),
            ])
            # Отправляем запрос на подвержение переноса дедлайна
            bot.send_message(task.director.chat_id, '\n'.join([msg, f'Задача: {task.text}']),
                             reply_markup=keyboard)

            # Сохранем данные в истории задач
            task_history.need_action = False
            task_history.save()
            teacher.status = 0
            teacher.save()
            bot.send_message(chat_id, 'Запрос на перенос дедлайна отправлен постановщику',
                             reply_markup=utils.get_main_menu_keyboard())
        # Воода даты дедлайна
        elif teacher.status == INPUT_DEADLINE_MODE:
            # Если не удалось преобразовать в дату, сообщаем об ошибке
            try:
                deadline = datetime.strptime(text, "%d.%m.%Y")
                if deadline.date() < now.date():
                    bot.send_message(chat_id, f'Недопустимая дата\n'
                                              f'Дедлайн не может быть раньше {now.strftime("%d.%m.%Y")}')
                    return
            except ValueError as e:
                bot.send_message(chat_id, 'Неверный формат даты, поробуйте еще раз')
                return
            # Получаем задачу, которая заполняется
            # Если задача не найдена, работает с TaskHistory
            try:
                task = Task.objects.get(director=teacher, status=-1)
                # Устанавливаем дедлайн в дату, которую ввели
                task.deadline = deadline
                # Переходим в режим ввода сути заачи
                task.save()
                teacher.status = INPUT_PROBLEM_MODE
                bot.send_message(chat_id, 'Напишите задачу и отправьте её боту',
                                 reply_markup=utils.get_keyboard(['❌ Отмена']))
            except Task.DoesNotExist:
                task_history = TaskHistory.objects.get(task__executor=teacher, need_action=True)
                task = task_history.task
                delta = deadline.date() - now.date()
                # Если это самозадача, то просто переносим дедлайн
                if task_history.task.director == teacher:
                    task_history.comment = f'{teacher} перенес дедлайн на {delta.days} дней'
                    task_history.need_action = False
                    task_history.save()
                    task.deadline = deadline
                    task.save()
                    teacher.status = 0
                    bot.send_message(chat_id, 'Дедлайн перенесен', reply_markup=utils.get_main_menu_keyboard())
                    return
                task_history.info = json.dumps({'days': delta.days})
                task_history.save()
                teacher.status = INPUT_REASON_MODE
                bot.send_message(chat_id, 'Напишите причину и отправьте её боту',
                                 reply_markup=utils.get_keyboard(['❌ Отмена']))

            teacher.save()
        # Ввод филиала для заказа деталей
        elif teacher.status == INPUT_BRANCH_FOR_ORDER:
            branch = utils.find_branch(text)
            if not branch:
                bot.send_message(chat_id, 'Филиал не найден')
                return
            # Если сегодня была заявка на зказа на этом филиале
            if Order.objects.filter(teacher=teacher, created__date=now, branch=branch):
                order = Order.objects.get(teacher=teacher, created__date=now, branch=branch)
                Order.objects.get(teacher=teacher, branch=None).delete()
            # Иначе получаем заказ, где не указан филиал
            else:
                order = Order.objects.get(teacher=teacher, branch=None)
                order.branch = branch
                order.save()

            url_for_order = (settings.ORDER_SERVICE_URL +
                             f'?callback=https://{settings.ALLOWED_HOSTS[0]}/order_callback/{order.token}/')
            teacher.status = 0
            teacher.save()
            bot.send_message(chat_id,
                             f'Для заказа деталей перейдите по ссылке: {url_for_order}',
                             reply_markup=utils.get_main_menu_keyboard())

        # если пользователь отправил фото ( после отправки первого фото status = 1)
        elif teacher.status == 1 and text.lower() != 'подготовка':
            text = text.capitalize()
            temp_log = None
            bot.send_message(chat_id, 'Ищем филиал...')

            branch = utils.find_branch(text)
            if branch:
                bot.send_message(chat_id, 'Филиал {} найден'.format(branch.name),
                                 reply_markup=types.ReplyKeyboardRemove())
            else:
                bot.send_message(chat_id, 'Филиал не найден, повторите попытку')
                return

            try:
                # ищем запись во временной таблице
                temp_log = TempData.objects.get(teacher=teacher)
            except Exception as e:
                bot.send_message(
                    chat_id, """
                Что-то пошло не так {}.
                Перешлите это сообщение @stas12312""".format(e))
                return
            try:
                if LogSending.objects.filter(teacher=teacher, date=temp_log.date, branch=branch).count():
                    log = LogSending.objects.get(teacher=teacher, date=temp_log.date, branch=branch)
                    move_files(
                        os.path.join(TEMP_DIR, teacher.slug_name),
                        os.path.join(settings.BASE_DIR, log.photos))
                    bot.send_message(chat_id, 'Фотографии успешно добавлены',
                                     reply_markup=utils.get_main_menu_keyboard())
                else:
                    # вычисляем ближайшее занятие
                    date = temp_log.date
                    time = temp_log.time

                    day_of_weak = date.weekday() + 1  # определяем день недели

                    # Получаем интервал для поиска занятий в этом интервале
                    interval_for_lessons = Settings.get_value('interval_for_lessons')
                    lessons = get_lesson(day_of_weak, time, branch, interval_for_lessons)

                    # если нет занятий или нет ближайших занятий
                    if not lessons:
                        # относительный путь до постоянной дирректорий
                        path = os.path.join(
                            settings.MEDIA_DIR,
                            settings.IMAGE_DIR,
                            branch.slug_name,
                            teacher.slug_name,
                            date.strftime('_%d-%m-%Y'))

                        # абсолютный путь до временной директории
                        from_path = os.path.join(TEMP_DIR, teacher.slug_name)
                        # абсолютный путь до постоянной директории
                        to_path = os.path.join(settings.BASE_DIR, path)

                        # перемещаем файлы из временной директории в постоянную
                        move_files(from_path, to_path, delete=True)
                        log = LogSending(date=date, time=time, teacher=teacher,
                                         branch=branch, photos=path)
                        log.save()
                        bot.send_message(chat_id,
                                         'В ближайшее время занятий не найдено, но фото сохранены',
                                         reply_markup=utils.get_main_menu_keyboard())
                    else:
                        # если нашлось несколько ближайших занятий, то формируем клавиатуры для выбора
                        if len(lessons) > 1:
                            # сохраняем филиал во временной записи
                            temp_log.branch = branch
                            temp_log.save()
                            teacher.status = 2
                            teacher.save()

                            keyboard = get_keyboard(lessons)
                            bot.send_message(
                                chat_id,
                                'Нашлось несколько ближайших занятий, выберите нужное',
                                reply_markup=keyboard
                            )
                            return

                        path = save_temp_files(teacher, branch, temp_log)
                        log = LogSending(date=date, time=time, teacher=teacher, branch=branch,
                                         lesson=lessons[0], photos=path)
                        log.save()

                        if log.time_before < 0:
                            msg = text_after_lesson
                        else:
                            msg = text_before_lesson

                        bot.send_message(
                            chat_id,
                            msg.format(log.date, log.time.strftime("%H:%M"), log.lesson, log.time_before),
                            reply_markup=utils.get_main_menu_keyboard())

                    # ================= Информирование администраторов о новых фото =========================== #
                    send_notification(log)  # отправляем уведомление о прибытии преподавателя

                # обнуляем статус у преподавателя
                temp_log.delete()  # удаляем временную запись
                teacher.status = 0
                teacher.save()
            except Exception as e:
                bot.send_message(chat_id, 'Возникла ошибка: {}'.format(e),
                                 reply_markup=utils.get_main_menu_keyboard())
        # обработка сортировки
        elif teacher.status == 3:
            # Обработка ввода филиала после ввода филиала
            branch = utils.find_branch(text)
            if not branch:
                bot.send_message(
                    chat_id,
                    'Филиал не найден, посмотрите филиалы выбрав "🏢 Филиалы"')
                return
            bot.send_message(chat_id, 'Филиал {} найден'.format(branch.name),
                             reply_markup=utils.get_main_menu_keyboard())

            try:
                temp_log = TempData.objects.get(teacher=teacher)
            except TempData.MultipleObjectsReturned:
                bot.send_message(chat_id,
                                 'Упс, что-то пошло не так\nИспользуйте команду /cancel, а затем повторите попытку')
                return

            if temp_log.counter == 0:
                bot.send_message(
                    chat_id,
                    'Вы не загрузили фото. Загрузите фото или введите /cancel для отмены')
                return
            date = temp_log.date
            time = temp_log.time
            # Относительный путь до хранения сортировки
            path = os.path.join(
                settings.MEDIA_DIR,
                'sorting',
                branch.slug_name,
                teacher.slug_name,
                '{}_{}'.format(date.strftime('%d-%m-%Y'), time.strftime('%H_%M')))

            from_path = os.path.join(TEMP_DIR, teacher.slug_name)
            to_path = os.path.join(settings.BASE_DIR, path)

            # Если в этот день не было сортировки, то создаем его
            if not Sort.objects.filter(date=date, branch=branch, status=0, teacher=teacher).count():
                Sort.objects.create(
                    teacher_id=teacher.id,
                    branch_id=branch.id,
                    date=date,
                    time=time,
                    count=temp_log.counter,
                    path=path,
                )
            else:
                sort = Sort.objects.get(date=date, branch=branch, teacher=teacher)
                sort.count += temp_log.counter
                sort.save()
                to_path = os.path.join(settings.BASE_DIR, sort.path)

            move_files(from_path, to_path, delete=True)
            teacher.status = 0
            teacher.save()
            temp_log.delete()
            bot.send_message(chat_id, 'Сортировка принята',
                             reply_markup=utils.get_main_menu_keyboard())
        # Обработка отправки фото с занятий
        elif teacher.status == 10:
            info = text.split(':')
            # Обрабатываем случай, когда пользователь сам попытался ввести данные
            logs = utils.get_branches_on_weak(timezone.localtime(timezone.now()) + timedelta(days=1), teacher)
            keyboard = utils.get_keyboard(list(set([log.branch.name for log in logs])) + [['🏢 Филиалы', '⬅️ Назад']])
            if len(info) != 2:
                bot.send_message(chat_id,
                                 'Неверный формат, выберите нужный филиал и дату с помощью клавиатуры',
                                 reply_markup=keyboard)
                return
            branch_name = info[0]
            date_ = info[1]
            # Если филиал существует, то отправляем информацию
            # в сервис для загрузки фото на Google Drive
            if Branch.objects.filter(name=branch_name).count():
                branch = Branch.objects.get(name=branch_name)
                # Перемещаем файлы из временной дирректории в другое временное хранилище
                from_path = os.path.join(
                    TEMP_DIR,
                    teacher.slug_name,
                )
                random_dir = secrets.token_hex(16)

                to_path = os.path.join(
                    TEMP_DIR,
                    random_dir
                )
                move_files(from_path, to_path, delete=True)

                # Получаем директория с фото
                temp_path = os.path.join(
                    'media/temp',
                    random_dir)
                links = utils.get_images(temp_path)
                links = [f'https://{settings.ALLOWED_HOSTS[0]}/{link}' for link in links]
                # Формируем словарь с информацией
                data = {
                    'teacher': teacher.get_short_name,
                    'branch': branch_name,
                    'date': date_,
                    'images': links,
                    'id': branch.num_code,
                    'callback': f'https://{settings.ALLOWED_HOSTS[0]}/photo_callback/{chat_id}/{temp_path}'
                }
                headers = {
                    'Content-Type': 'application/json',
                }
                url = f'https://ligajournal.ru/service/upload/{settings.DRIVE_FOLDER}/'
                try:
                    session = requests.Session()
                    r = session.post(url=url, data=json.dumps(data), headers=headers)
                    if r.status_code == 200:
                        bot.send_message(chat_id, 'Фотографии загружаются на Google диск'
                                                  ', по завершению загрузки вам придёт уведомление',
                                         reply_markup=utils.get_main_menu_keyboard())
                        temp_data = TempData.objects.get(teacher=teacher)

                        photo_log = PhotoLog.objects.filter(teacher=teacher,
                                                            branch=branch,
                                                            photo_created=datetime.strptime(date_.strip(),
                                                                                            '%d.%m.%Y'))
                        # Добавляем информацию в БД если нет отчетов
                        if not photo_log.count():
                            PhotoLog.objects.create(
                                teacher=teacher,
                                branch=branch,
                                photo_created=datetime.strptime(date_.strip(), '%d.%m.%Y'),
                                photo_count=temp_data.counter
                            )
                        else:
                            ph = photo_log[0]
                            ph.photo_count += temp_data.counter
                            ph.save()

                        # Удаляем временную запись
                        temp_data.delete()
                    else:
                        bot.send_message(chat_id,
                                         'Ошибка при отправке, повторите попытку позже',
                                         reply_markup=utils.get_main_menu_keyboard())
                        proc_cancel_text(message)
                    teacher.status = 0
                    teacher.save()
                except Exception as e:
                    print(e)
    # если пользователь не авторизован
    elif new_user:
        # авторизуем пользователя (status равен 1 после ввода команды /auth )
        if new_user.status == 1:
            text = text.title()
            bot.send_message(chat_id, 'Ищем вас в Базе данных...')

            try:
                teacher = Teacher.objects.get(full_name=text, is_active=True)
                if teacher.chat_id:
                    bot.send_message(chat_id,
                                     'Данный пользователь уже авторизован',
                                     reply_markup=utils.get_keyboard(['🔑 Авторизация']),
                                     )
                    return

                teacher.chat_id = chat_id
                teacher.save()
                welcome_message = Settings.get_value('welcome_message', )
                bot.send_message(
                    chat_id, welcome_message.format(name=teacher.full_name),
                    parse_mode='HTML', reply_markup=utils.get_main_menu_keyboard())
                new_user.delete()

            # если преподаватель не найден, то информируем об этом
            except Teacher.DoesNotExist:
                bot.send_message(
                    chat_id, """
Упс, мы вас не нашли.
Повторите попытку /auth или обратитесь к @stas12312
""", reply_markup=utils.get_keyboard(['🔑 Авторизация']))
                new_user.status = 0
                new_user.save()

    # если что-то пошло не так)
    else:
        bot.send_message(
            chat_id, """
Если вы видете это сообщение, то что-то пошло не так.
Вы знаете куда обращаться @stas12312
                """
        )


# ======================= ПРИЁМ ФОТОГРАФИЙ ==================== #
@bot.message_handler(content_types=['photo'])
@utils.teacher_required(bot=bot)
def proc_document(message):
    chat_id = message.chat.id  # получаем id отправителя
    teacher = Teacher.objects.get(chat_id=chat_id)
    try:
        # Если первая фотография была отправлена документом
        if teacher.status == 20:
            bot.send_message(chat_id, 'Фотографии должны быть загружены документом')
            return
        if teacher.status == INPUT_PROBLEM_MODE or teacher.status == CHOICE_DEPARTAMENT_MODE:
            bot.send_message(chat_id, 'Вы не закончили с постановкой задачи')
            return

        # если это первое фото, то заносим информацию  о нем в БД
        if teacher.status == 0 or not TempData.objects.filter(teacher=teacher).count():
            # ДОБАВИТЬ ОБРАБОТКУ СТАРЫХ ЛОГОВ, ЕСЛИ ОНИ ЕСТЬ
            teacher.status = 1
            teacher.save()
            # Блокировка БД
            with lock_table(TempData):
                #  добавляем данные во таблицу временных данных
                if not TempData.objects.filter(teacher=teacher).count():
                    TempData.objects.create(
                        date=timezone.localtime(timezone.now()),
                        time=datetime.time(timezone.localtime(timezone.now())),
                        teacher_id=teacher.id,
                    )

            # отправляем информацию о времени приема первой фотографии
            time = timezone.localtime(timezone.now()).strftime('%H:%M')
            bot.send_message(chat_id, 'Зафиксированное время: {}'.format(time),
                             reply_markup=utils.get_action_keyboard(utils.ACTION_MODE_PHOTO))
        try:
            temp = TempData.objects.get(teacher=teacher)
            temp.counter += 1
            temp.save()
        except TempData.MultipleObjectsReturned:
            bot.send_message(chat_id,
                             'Упс, что-то пошло не так\nИспользуйте команду /cancel, '
                             'а затем повторите попытку')
            return
        # сохраняем фото
        file_info = bot.get_file(message.photo[-1].file_id)
        save_file(file_info, teacher.slug_name)

        bot.reply_to(message, 'Фото загружено')
        if message.caption:
            proc_all_messages(message)
    except Exception as e:
        bot.send_message(
            chat_id,
            'Ошибка:{}\n{}'.format(e, traceback.format_exc()))


# ===================== ПРИЕМ ФОТО В ВИДЕ ДОКУМЕНТА ========================= #
@bot.message_handler(content_types=['document'])
@utils.teacher_required(bot=bot)
def proc_document(message):
    chat_id = message.chat.id
    mime_type = message.document.mime_type
    teacher = Teacher.objects.get(chat_id=chat_id)
    if not settings.DRIVE_FOLDER:
        bot.send_message(chat_id, 'Данный функционал вам недоступен')
        return

    if mime_type not in available_mime_types:
        bot.reply_to(message, 'Неподдерживаемый тип документа')
        return

    if teacher.status == 0 or not TempData.objects.filter(teacher=teacher).count():
        teacher.status = 1
        teacher.save()
        with lock_table(TempData):
            #  добавляем данные во таблицу временных данных
            if not TempData.objects.filter(teacher=teacher).count():
                TempData.objects.create(
                    date=timezone.localtime(timezone.now()),
                    time=datetime.time(timezone.localtime(timezone.now())),
                    teacher_id=teacher.id,
                )
        bot.send_message(chat_id, 'Загрузка фотографий', reply_markup=utils.get_action_keyboard(utils.ACTION_MODE_FILE))
    # сохраняем фото
    file_info = bot.get_file(message.document.file_id)
    save_file(file_info, teacher.slug_name)

    try:
        # Увеличиваем счетчик фотографий
        temp = TempData.objects.get(teacher=teacher)
        temp.counter += 1
        temp.save()
    except TempData.MultipleObjectsReturned:
        bot.send_message(chat_id,
                         'Упс, что-то пошло не так\nИспользуйте команду /cancel, '
                         'а затем повторите попытку')
        return

    bot.reply_to(message, 'Фото загружено')
    if message.caption:
        proc_all_messages(message)


@bot.message_handler(content_types=['video'])
@utils.teacher_required(bot=bot)
def proc_video(message):
    bot.reply_to(message, 'Загрузка видео не поддерживается')
