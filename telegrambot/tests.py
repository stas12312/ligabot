from django.test import TestCase

from datetime import datetime
from .models import Lesson, Branch
from .bot import get_lesson


# Create your tests here.
class Testa(TestCase):
    @classmethod
    def setUpTestData(cls):
        lesson1 = Lesson.objects.create(name="Тестовое 1",
                                        start=datetime(0, 0, 0, 14, 0),
                                        end=datetime(0, 0, 0, 15, 0),
                                        day_of_weak=1)

        lesson2 = Lesson.objects.create(name="Тестовое 2",
                                        start=datetime(0, 0, 0, 16, 0),
                                        end=datetime(0, 0, 0, 17, 0),
                                        day_of_weak=1)

        lesson3 = Lesson.objects.create(name="Тестовое 3",
                                        start=datetime(0, 0, 0, 14, 0),
                                        end=datetime(0, 0, 0, 15, 0),
                                        day_of_weak=2)

        branch = Branch.objects.create(name="Тестовый")
        branch.lessons.add(lesson1)
        branch.lessons.add(lesson2)
        branch.lessons.add(lesson3)
        branch.save()

    def test_collision(self):
        day_of_weak = 1
        delta = 120
        branch = Branch.objects.get(id=1)
        now = datetime(0, 0, 0, 13, 10)
        result = get_lesson(day_of_weak, branch, now, delta)
        self.assertEquals(len(result), 2)
